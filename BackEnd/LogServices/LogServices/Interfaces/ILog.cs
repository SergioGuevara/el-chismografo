using LogServices.Models;
using System.Collections.Generic;

namespace LogServices.Interfaces
{
    public interface ILog
    {
        string SaveLog(LogModel element);

        void AddTask (string idLog, TaskModel task);
    }
}