using LogServices.Interfaces;
using LogServices.DAL.Controller;
using LogServices.DAL.Interfaces;
using System.Collections.Generic;
using LogServices.Models;

namespace LogServices.Controller
{
    internal class LogController : ILog
    {
        public IContext _context { get; set; }

        public LogController()
        {
            if(_context == null){
                DataFactory factory = new DataFactory();
                _context = factory.GetDataInstance();
            }
        }

        public string SaveLog(LogModel element){
            return _context.Logs.Save(element);
        }

        public void AddTask(string idLog, TaskModel task){
            var log = _context.Logs.Find(idLog);
            if(log.Tasks == null)
                log.Tasks = new List<TaskModel>();
            log.Tasks.Add(task);
            _context.Logs.Update(log);
        }
    }
}