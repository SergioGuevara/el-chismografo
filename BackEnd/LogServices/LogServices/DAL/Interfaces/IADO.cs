using System.Collections.Generic;

namespace LogServices.DAL.Interfaces
{
    public interface IADO<T>
    {

        List<T> ToList();

        T Find(string Id);
    
        void Update(T entity);

        string Save(T entity);
    }
}