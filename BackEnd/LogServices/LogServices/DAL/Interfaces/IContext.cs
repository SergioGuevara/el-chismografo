using LogServices.Models;
using LogServices.DAL.Interfaces;

namespace LogServices.DAL.Interfaces
{
    public interface IContext
    {
        IADO<LogModel> Logs { get; set; }

    }
}