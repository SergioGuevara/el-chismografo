using System;
using LogServices.Models.Util;

namespace LogServices.DAL.Controller
{
    public class DataFactory
    {
        public LogServices.DAL.Interfaces.IContext GetDataInstance(){
            //Type typetoreturn = Type.GetType(Constants.C_LS_TypeProviderName);//"LogServices.DAL.DAOs.MongoDB.MongoDBContext,Context"
            Type typetoreturn = Type.GetType("LogServices.DAL.DAOs.MongoDB.MongoDBContext,LogServices");
            LogServices.DAL.Interfaces.IContext oReturn = (LogServices.DAL.Interfaces.IContext)Activator.CreateInstance(typetoreturn);
            return oReturn;
        }
    }
}