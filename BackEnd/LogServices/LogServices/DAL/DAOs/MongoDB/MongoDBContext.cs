using LogServices.DAL.Interfaces;
using LogServices.Models;
using MongoDB.Driver;
using MongoDB.Bson;

namespace LogServices.DAL.DAOs.MongoDB
{
    internal class MongoDBContext : IContext
    {
        public MongoDBContext()
        {
        }

        public IConnection<MongoDBConnection> _connection { get; set; }

        public IADO<LogModel> Logs { get{
            return new MongoDB_ADO<LogModel>("log");
        } set {} }
    }
}