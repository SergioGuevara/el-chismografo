using LogServices.DAL.Interfaces;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Collections.Generic;
using LogServices.DAL.DAOs.MongoDB.Interfaces;

namespace LogServices.DAL.DAOs.MongoDB
{
    internal class MongoDB_ADO<T> : IADO<T> where T:IModel
    {
        private MongoDBConnection connection { get; set; }
        private  IMongoCollection<T> _collection;

        public MongoDB_ADO(string collection)
        {
            connection = new MongoDBConnection();
            if( _collection == null){
                _collection = this.getCollection(collection);
            }
        }

        private IMongoCollection<T> getCollection(string collectionName) {
            return connection._database.GetCollection<T>(collectionName);
        } 

        public List<T> ToList(){
            return _collection.Find(new BsonDocument()).ToList();
        }

        public T Find(string Id){
            ObjectId ObjId = new ObjectId(Id);
            return _collection.Find(b => b.Id == ObjId).FirstAsync().Result;
        }

        public void Update(T entity){
            _collection.ReplaceOneAsync(
                i => i.Id == entity.Id,
                entity,
                new ReplaceOptions { IsUpsert = true }).Wait();
        }

        public string Save(T entity){
            _collection.InsertOneAsync(entity).Wait();
            return entity.Id.ToString();
        }


    }
}