using MongoDB.Bson;

namespace LogServices.DAL.DAOs.MongoDB.Interfaces
{
    internal interface IModel
    {
        ObjectId Id { get; set; }
    }
}