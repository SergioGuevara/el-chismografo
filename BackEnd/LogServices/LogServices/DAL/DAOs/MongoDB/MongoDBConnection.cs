using LogServices.DAL.Interfaces;
using MongoDB.Driver;
using LogServices.Models.Util;

namespace LogServices.DAL.DAOs.MongoDB
{
    internal class MongoDBConnection : IConnection<IMongoDatabase>
    {
        public IMongoDatabase _database { get; set; }

        public MongoDBConnection()
        {
            var client = new MongoClient();
            //_database = client.GetDatabase(Constants.C_LS_ProviderConnectionName); //"chismografo_log"
            _database = client.GetDatabase("chismografo"); 
        }

    }
}