using System.Collections.Generic;
using LogServices.Models;
using LogServices.Interfaces;
using LogServices.Controller;
using Autofac;

namespace LogServices.Client
{
    public static class LogClient
    {

        public static string SaveLog(LogModel entity){
            var builder = new ContainerBuilder();
            builder.RegisterType<LogController>().As<ILog>();

            var container = builder.Build();
            return container.Resolve<ILog>().SaveLog(entity);
        }

        public static void AddTask(string iddLog, TaskModel task){
            var builder = new ContainerBuilder();
            builder.RegisterType<LogController>().As<ILog>();

            var container = builder.Build();
            container.Resolve<ILog>().AddTask(iddLog, task);
        }
    }
}