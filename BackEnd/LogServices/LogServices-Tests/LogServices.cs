using System;
using Xunit;
using LogServices.Client;
using LogServices.Models;


namespace LogServices_Tests
{
    public class LogServices
    {

        [Fact]
        public void Save()
        {
            LogModel n = new LogModel(){
                Process = "Registro",
                User = "sergio@",
                Tasks = new System.Collections.Generic.List<TaskModel>(){
                    new TaskModel() { test = "1" },
                    new TaskModel() { test = "2" },
                    new TaskModel() { test = "3" },
                    new TaskModel() { test = "4" }
                },
                Date = DateTime.Now
            };
            var oResult = LogClient.SaveLog(n);
        }

        [Fact]
        public void AddTask()
        {
            TaskModel task = new TaskModel(){
                test = "tres"
            };
            LogClient.AddTask("5f0e50c1e4265e20bfd98cc9", task);
        }
    }
}
