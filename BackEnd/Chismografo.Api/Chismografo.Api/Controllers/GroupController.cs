using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Chismografo.Core.Models;
using Chismografo.Core.Client;
using Chismografo.Api.Models.Utilities;
using System;
using Chismografo.Api.Models;
//using EnumerationsCore = Chismografo.Core.Models.Util;
using System.Collections.Generic;
using System.Linq;

namespace Chismografo.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GroupController : ControllerBase
    {
        public string id_user { get; set; }
        ResponseModel obResponse = new ResponseModel() { };
        private readonly ILogger<GroupController> _logger;

        public GroupController(ILogger<GroupController> logger)
        {
            _logger = logger;
            this.id_user = "969885f3-7db0-443d-8b0a-0215f8df59b6";
        }

        #region Group Methods

        /// <summary>
        /// Expone la creación de un grupo para un usuario determinado y si tiene listado de invitados envia la invitación
        /// </summary>
        /// <param name="group">Modelo de grupo ha crear</param>
        /// <returns>Modelo respuesta ResponseModel (Modelo de grupo creado)</returns>
        [HttpPost("create")]
        public IActionResult CreateGroup(GroupViewModel group)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                var oResult = GroupClient.CreateGroup(new GroupModel(){
                    name = group.name,
                    creator_user = new UserModel(){
                        id_public = group.creator_user.id_public
                    }
                });

                obResponse.DataResponse = new Models.GroupViewModel(oResult);
                obResponse.Message = "Se registro correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Edita la info de un grupo 
        /// </summary>
        /// <param name="group">Modelo de grupo ha modificar</param>
        /// <returns>Modelo de grupo creado</returns>
        [HttpPost("edit")]
        public IActionResult EditGroup(GroupViewModel group)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;

                GroupModel _group = new GroupModel(){
                    id_public = group.id_public,
                    name = group.name
                };

                obResponse.DataResponse = new GroupViewModel(GroupClient.EditGroup(id_user, _group));
                obResponse.Message = "La ha editado el grupo correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la eliminación de un grupo
        /// </summary>
        /// <param name="id_public">Identificación publica del grupo</param>
        [HttpPost("delete/{id_public}")]
        public IActionResult DeleteGroup(string id_public)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                GroupClient.DeleteGroup(this.id_user, id_public);

                obResponse.DataResponse = null;
                obResponse.Message = "Se ha eliminado el grupo correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la funcionalidad de obtener un grupo
        /// </summary>
        /// <param name="id_public">id publico del grupo</param>
        /// <returns>Modelo del grupo encontrado</returns>
        [HttpGet("{id_public}")]
        public IActionResult GetGroup(string id_public)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;

                GroupViewModel data = new GroupViewModel(GroupClient.GetGroup(id_public));
                Tuple<GroupViewModel, int> dataResult = new Tuple<GroupViewModel, int>(data, GroupClient.GetGroupsOfUser(this.id_user).Count);

                obResponse.DataResponse = dataResult;
                obResponse.Message = "Se encontro el siguiente grupo.";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Obtiene los grupos del usuario logeado
        /// </summary>
        /// <returns>Listado de modelos del grupos de un usuario</returns>
        [HttpGet("getAll")]
        public IActionResult GetGroups()
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;

                List<GroupViewModel> oReturn = new List<GroupViewModel>();

                List<GroupModel> oResult = GroupClient.GetGroupsOfUser(id_user);

                oResult?.All( x => {
                    oReturn.Add(new GroupViewModel(x));
                    return true;
                });

                obResponse.DataResponse = oReturn;
                obResponse.Message = "Se encontraron los siguientes grupos.";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la funcionalidad de Salida de un miembre de un grupo
        /// </summary>
        /// <param name="id_user">id del usuario</param>
        /// <param name="id_group">id del grupo</param>
        /// <returns>Modelo respuesta ResponseModel (Valor Bool para identificar si la tarea se hizo correctamente)</returns>
        [HttpPost("leave/{id_group}")]
        public IActionResult LeaveGroup(string id_group)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                var oResult = GroupClient.LeaveGroup(this.id_user, id_group);

                obResponse.DataResponse = new Models.GroupViewModel(oResult);
                obResponse.Message = "La salida del grupo fue exitosa!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone el envio de la invitación a usuarios para unirsen a un grupo (App/Email/Facebook)
        /// </summary>
        /// <param name="group">Modelo de grupo al cual se realizará la invitación</param>
        /// <returns>Modelo respuesta ResponseModel (Valor Bool para identificar si la tarea se hizo correctamente)</returns>
        [HttpPost("sendinvitation")]
        public IActionResult SendInvitation(string id_group)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                var oResult = GroupClient.SendInvitation(id_group, this.id_user, Core.Models.Util.Enumerations.EnumInvitationType.App);

                obResponse.DataResponse = oResult;
                obResponse.Message = "Se envió la invitación correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la generación del link de invitación a un grupo
        /// </summary>
        /// <param name="group">Modelo de grupo al cual se le generá el link de invitación</param>
        /// <returns>Modelo respuesta ResponseModel (Link de invitación)</returns>
        [HttpPost("getinvitationlink/{id_group}")]
        public IActionResult GetInvitationLink(string id_group)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;

                obResponse.DataResponse = GroupClient.GetInvitationLink(id_group);
                obResponse.Message = "Se obtuvó el link correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        #endregion

        #region Question Methods

        /// <summary>
        /// Expone la creación de una pregunta dentro de un grupo
        /// </summary>
        /// <param name="id_question">Modelo de la pregunta que se va ha agregar al grupo</param>
        /// <returns>Modelo del grupo afectado</returns>
        [HttpPost("create-question")]
        public IActionResult CreateQuestion(QuestionViewModel question){
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                QuestionModel _question = new QuestionModel(){
                    question = question.question,
                    category = new CategoryQuestionModel(){
                        id_public = question.category.id_public
                    },
                };

                obResponse.DataResponse = new GroupViewModel(GroupClient.CreateQuestion(question.id_public_group, _question));
                obResponse.Message = "Se creó la pregunta correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Exponde la modificación de una pregunta dentro de un grupo
        /// </summary>
        /// <param name="question">Modelo de la pregunta que se va ha modificar</param>
        /// <returns>Modelo del grupo afectado</returns>
        [HttpPost("edit-question")]
        public IActionResult EditQuestion(QuestionViewModel question)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                QuestionModel _question = new QuestionModel(){
                    id_public = question.id_public,
                    question = question.question,
                    category = new CategoryQuestionModel(){
                        id_public = question.category.id_public,
                    },
                };

                obResponse.DataResponse = new Models.GroupViewModel(GroupClient.EditQuestion(_question));
                obResponse.Message = "Se actualizó la pregunta correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la eliminación de una pregunta dentro de un grupo
        /// </summary>
        /// <param name="id_group">Identificación publica del grupo</param>
        /// <param name="id_question">Identificación publica de una pregunta que se va ha elimnar del grupo</param>
        /// <returns>Modelo del grupo afectado</returns>
        [HttpPost("delete-question/{id_question}")]
        public IActionResult DeleteQuestion(string id_question)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                GroupClient.DeleteQuestion(this.id_user, id_question);

                obResponse.DataResponse = true;
                obResponse.Message = "Se creó la pregunta correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la eliminación de una pregunta dentro de un grupo
        /// </summary>
        /// <param name="id_group">Identificación publica del grupo</param>
        /// <param name="id_question">Identificación publica de una pregunta que se va ha elimnar del grupo</param>
        /// <returns>Modelo del grupo afectado</returns>
        [HttpGet("question/{id_question}")]
        public IActionResult GetQuestion(string id_question)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                obResponse.DataResponse = new QuestionViewModel(GroupClient.GetQuestion(id_question));
                obResponse.Message = "Se encontro la siguiente pregunta!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        #endregion

        #region Answer 

        /// <summary>
        /// Expone creación de una respuestaha una pregunta
        /// </summary>
        /// <param name="answer">Modelo de la respuesta</param>
        /// <returns>Modelo del grupo afectado</returns>
        [HttpPost("answer")]
        public IActionResult AnswerQuestion(AnswerViewModel answer)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                AnswerModel _answer = new AnswerModel(){
                    text = answer.text
                };

                obResponse.DataResponse = new Models.GroupViewModel(GroupClient.AnswerQuestion(answer._id_public_question, this.id_user, _answer));
                obResponse.Message = "Se creó la pregunta correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la eliminación de una resputa
        /// </summary>
        /// <param name="id_answer">Identificación de la respuesta</param>
        /// <param name="id_user">Identificación publica del usuario se encuentra logeoado</param>
        /// <returns>Modelo del grupo afectado</returns>
        [HttpPost("delete-answer/{id_public}")]
        public IActionResult DeleteAnswer(string id_public)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;

                obResponse.DataResponse = new Models.GroupViewModel(GroupClient.DeleteAnswer(this.id_user, id_public));
                obResponse.Message = "Se eliminó la respuesta correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Exponde la edición de una resputa
        /// </summary>
        /// <param name="id_user">Identificación publica del usuario se encuentra logeoado</param>
        /// <param name="answer">Modelo de la respuesta</param>
        /// <returns>Modelo del grupo afectado</returns>
        [HttpPost("edit-answer")]
        public IActionResult EditAnswer(AnswerViewModel answer)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                AnswerModel _answer = new AnswerModel(){
                    text = answer.text
                };

                obResponse.DataResponse = new Models.GroupViewModel(GroupClient.EditAnswer(this.id_user, _answer));
                obResponse.Message = "Se creó la pregunta correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone el listado de respuestas para una pregunta
        /// </summary>
        /// <param name="id_question">Identificación de la pregunta</param>
        /// <returns>Listado de resputas </returns>
        [HttpGet("get-answers/{id_question}")]
        public IActionResult GetAnswers(string id_question)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;

                List<AnswerViewModel> Response = new List<AnswerViewModel>();
                List<AnswerModel> oResult = GroupClient.GetAnswers(id_question);
                oResult?.All(x => {
                    Response.Add(new AnswerViewModel(x));
                    return true;
                });

                obResponse.DataResponse = Response;
                obResponse.Message = "Se obtuvieron los siguientes registros!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        #endregion

    }
}