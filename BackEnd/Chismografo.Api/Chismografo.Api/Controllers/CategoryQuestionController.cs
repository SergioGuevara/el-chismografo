using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Chismografo.Core.Models;
using Chismografo.Core.Client;
using Chismografo.Api.Models.Utilities;
using System;
using System.Linq;
using System.Collections.Generic;
using Chismografo.Api.Models;

namespace Chismografo.Api.Controllers
{
    [ApiController]
    [Route("")]
    public class CategoryQuestionController : ControllerBase
    {

        ResponseModel obResponse = new ResponseModel() { };
        private readonly ILogger<CategoryQuestionController> _logger;

        public CategoryQuestionController(ILogger<CategoryQuestionController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Expone la creación de una categoria de preguntas
        /// </summary>
        /// <param name="category">Modelo de una categoria a crear</param>
        /// <returns>Modelo respuesta ResponseModel (Modelo de la categoria creada)</returns>
        [HttpPost("category/create")]
        public IActionResult CreateCategoryQuestion(CategoryQuestionViewModel question)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                var oResult = CategoryQuestionClient.CreateCategoryQuestion(new CategoryQuestionModel(){
                    name = question.name
                });

                obResponse.DataResponse = new CategoryQuestionViewModel(oResult);
                obResponse.Message = "Se registro correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }
            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la modificación de una categoria de preguntas
        /// </summary>
        /// <param name="category">Modelo de una categoria a modificar</param>
        /// <returns>Modelo de la categoria modificadá</returns>
        [HttpPost("category/edit")]
        public IActionResult EditCategoryQuestion(CategoryQuestionViewModel category)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;

                CategoryQuestionModel _categoryModel = new CategoryQuestionModel(){
                    id_public = category.id_public,
                    name = category.name
                };
                
                obResponse.DataResponse = new CategoryQuestionViewModel(CategoryQuestionClient.EditCategoryQuestion(_categoryModel));
                obResponse.Message = "Se modificó correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Obtiene una categoria de preguntas
        /// </summary>
        /// <param name="id_public">Identificador publico de la categoria</param>
        /// <returns>Modelo de la categoria a obtener</returns>
       [HttpGet("category/{id_public}")] 
        public IActionResult GetCategoryQuestion(string id_public){
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                obResponse.DataResponse = new CategoryQuestionViewModel(CategoryQuestionClient.GetCategoryQuestion(id_public));
                obResponse.Message = "Se encontró correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la lista todas las categorias existentes, incluso las deshabilitadas
        /// </summary>
        /// <returns>Listado de categorias (CategoryQuestionModel)</returns>
        [HttpGet("categories")]
        public IActionResult getAll()
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;

                List<CategoryQuestionViewModel> oResponse = new List<CategoryQuestionViewModel>();

                var oResult = CategoryQuestionClient.getAll();
                oResult?.All(x=> {
                    oResponse.Add(new CategoryQuestionViewModel(x));
                    return true;
                });
                
                obResponse.DataResponse = oResponse;
                obResponse.Message = "Se registro correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }    

        /// <summary>
        /// Expone el cambio de estado de una categoria
        /// </summary>
        /// <param name="id">id_public de la categoria</param>
        /// <returns>La categoria afectada(CategoryQuestionModel)</returns>
        [HttpPost("category/changestatus")]
        public IActionResult changeStatus(string id)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                obResponse.DataResponse = new CategoryQuestionViewModel(CategoryQuestionClient.changeStatus(id));
                obResponse.Message = "Se registro correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }


    }
}