using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Chismografo.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : Controller
    {
        
        [HttpGet("index")]
        public IActionResult Index(){
            return View();
            /*return new JsonResult (new {
                test = "Client Home Page"
            });*/
        }

        [HttpGet("secret")]
        [Authorize]
        public JsonResult Secret(){
            return new JsonResult (new {
                test = "Client Secret Page"
            });
        }
    }
}