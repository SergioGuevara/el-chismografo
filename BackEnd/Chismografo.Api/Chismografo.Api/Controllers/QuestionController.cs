using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Chismografo.Core.Models;
using Chismografo.Core.Client;
using System.Net.Http;
using Chismografo.Api.Models.Utilities;
using System.Net;
using Newtonsoft.Json;
using System;
using Chismografo.Api.Models;
using System.Collections.Generic;
using System.Linq;

namespace Chismografo.Api.Controllers
{
    [ApiController]
    [Route("question")]
    public class QuestionController : ControllerBase
    {

        ResponseModel obResponse = new ResponseModel() { };
        private readonly ILogger<QuestionController> _logger;

        public QuestionController(ILogger<QuestionController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Expone la creación de una pregunta
        /// </summary>
        /// <param name="question">Modelo de la pregunta para crear</param>
        /// <returns>Modelo respuesta ResponseModel (Modelo de la pregunta creada)</returns>
        [HttpPost("create")]
        public IActionResult CreateQuestion(QuestionViewModel question)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                var oResult = QuestionClient.CreateQuestion(new QuestionModel(){
                    question = question.question,
                    category = new CategoryQuestionModel(){
                        id_public = question.category.id_public
                    },
                });

                obResponse.DataResponse = new Models.QuestionViewModel(oResult);
                obResponse.Message = "Se creó la pregunta correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }
        
        /// <summary>
        /// Expone la modificación de una pregunta
        /// </summary>
        /// <param name="question">Modelo de la pregunta a modificar</param>
        /// <returns>Modelo de la pregunta modificadá</returns>
        [HttpPost("edit")]
        public IActionResult EditQuestion(QuestionViewModel question){
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;

                QuestionModel _questionModel = new QuestionModel(){
                    id_public = question.id_public,
                    question = question.question,
                    category = new CategoryQuestionModel(){
                        id_public = question.category.id_public
                    }
                };
                
                obResponse.DataResponse = new QuestionViewModel(QuestionClient.EditQuestion(_questionModel));
                obResponse.Message = "Se modificó correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Obtiene una pregunta
        /// </summary>
        /// <param name="id_public">Identificador publico de la pregunta</param>
        /// <returns>Modelo de la pregunta a obtener</returns>
       [HttpGet("{id_public}")] 
        public IActionResult GetQuestion(string id_public){
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                obResponse.DataResponse = new QuestionViewModel(QuestionClient.GetQuestion(id_public));
                obResponse.Message = "Se encontró correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la Lista todas las preguntas, que no esten des habilitadas
        /// </summary>
        /// <returns>listado de preguntas (QuestionViewModel)</returns>
        [HttpGet("getEnabledQuestions")]
        public IActionResult getEnabledQuestions()
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;

                var oResult = QuestionClient.getEnabledQuestions();
                List<QuestionViewModel> oData = new List<QuestionViewModel>();

                oResult?.All( x => {
                    oData.Add(new QuestionViewModel(x));
                    return true;
                } );

                obResponse.DataResponse = oData;
                obResponse.Message = "Se encontraron los siguientes registros!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone el cambio de estado de una pregunta
        /// </summary>
        /// <param name="id">id_public de la pregunta</param>
        /// <returns>La pregunta afectada(QuestionViewModel)</returns>
        [HttpPost("changestatus")]
        public IActionResult changeStatus(string id)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                obResponse.DataResponse = new QuestionViewModel(QuestionClient.changeStatus(id));
                obResponse.Message = "Se registro correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }
    
    }
}