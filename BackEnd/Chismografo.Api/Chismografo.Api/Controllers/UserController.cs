using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Chismografo.Core.Models;
using Chismografo.Core.Client;
using System.Net.Http;
using Chismografo.Api.Models.Utilities;
using System.Net;
using Newtonsoft.Json;
using System;
using Chismografo.Api.Models;
using System.Collections.Generic;
using System.Linq;

namespace Chismografo.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        ResponseModel obResponse = new ResponseModel() { };
        private readonly ILogger<UserController> _logger;

        public UserController(ILogger<UserController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Expone la funcionalidad de registro de un nuevo usuario
        /// </summary>
        /// <param name="model">Modelo de la vista para realizar el registro</param>
        /// <returns>Modelo respuesta ResponseModel (Modelo de usuario registrado)</returns>
        [HttpPost("register")]
        public IActionResult Register(SocialViewModel model)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                var oResult = UserClient.Register(new UserModel(){
                    name = model.firstName,
                    lastname = model.lastName,
                    photo = model.photoUrl,
                    social_info = new SocialModel(){
                        id = model.id,
                        email = model.email,
                        provider = model.provider
                    },
                    email = model.email
                });

                obResponse.DataResponse = new Models.UserViewModel(oResult);
                obResponse.Message = "Se registro correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la funcionalidad de logeo
        /// </summary>
        /// <param name="model">Modelo de la vista para realizar el registro</param>
        /// <returns>Modelo respuesta ResponseModel (Modelo de usuario registrado)</returns>
        [HttpPost("login")]
        public IActionResult Login(SocialViewModel model)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                var oResult = UserClient.Login(new UserModel(){
                    social_info = new SocialModel(){
                        id = model.id
                    }
                });

                obResponse.DataResponse = new Models.UserViewModel(oResult);
                obResponse.Message = "Se registro el Inicio de session!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la funcionalidad de cerrar sessio
        /// </summary>
        /// <param name="model">Modelo de la vista para realizar el registro</param>
        /// <returns>Modelo respuesta ResponseModel (Modelo de usuario registrado)</returns>
        [HttpPost("logout")]
        public IActionResult Logout(SocialViewModel model)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;

                obResponse.DataResponse = UserClient.Logout(new UserModel(){
                    social_info = new SocialModel(){
                        id = model.id
                    }
                });
                obResponse.Message = "Se cerro session correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// </summary>
        /// <param name="user">Modelo de usuario para actualizar</param>
        /// <returns>Modelo respuesta ResponseModel (Modelo de usuario actualizado)</returns>
        [HttpPost]
        public IActionResult UpdateProfile(UserViewModel model)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                var oResult = UserClient.UpdateProfile(new UserModel(){
                    id_public = model.id_public,
                    document_type = new DocumentTypeModel() { name = "Cedula" },
                    identification = model.identification,
                    name = model.name,
                    lastname = model.lastname,
                    celphone = model.celphone,
                    email = model.email,
                    status = model.status
                });

                obResponse.DataResponse = new Models.UserViewModel(oResult);
                obResponse.Message = "Se actualizó la información correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la busqueda de usuarios activos
        /// </summary>
        /// <param name="pageNumber">Numero de pagina</param>
        /// <param name="pageSize">Tamaño de Pagina</param>
        /// <param name="search">Parametro de Busqueda</param>
        /// <returns>Modelo respuesta ResponseModel (Conteo de resultados y Listado de usuarios activos encontrados)</returns>
        [HttpGet]
        public IActionResult SearchUsers(int? pageNumber, int? pageSize, string search)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                List<UserViewModel> listUser = null;
                Tuple<int, List<UserModel>> result = UserClient.SearchUsers(pageNumber,pageSize,search);

                result.Item2.All( x =>
                {
                    if(listUser == null)
                        listUser = new List<UserViewModel>();
                    listUser.Add(new UserViewModel(x));
                    return true;
                });

                var oResult = new
                    {
                        Count = result.Item1,
                        Items = listUser
                    };

                obResponse.DataResponse = oResult;
                obResponse.Message = "Se encontraron los siguientes registros!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        // <summary>
        /// Expone las notificaciones sin leer (APP) para un usuario en especifico
        /// </summary>
        /// <param name="element">Modelo de Usuario en session</param>
        /// <returns>Lista de notificaciones</returns>
        [HttpGet("get-notifications")]
        public IActionResult GetNotofication(UserModel user)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;

                obResponse.DataResponse = UserClient.GetNotification(user);
                obResponse.Message = "Se encontraron los siguientes registros!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }

        /// <summary>
        /// Expone la Sincronización del usuario con una cuenta de Facebook
        /// </summary>
        /// <returns>Modelo respuesta ResponseModel ()</returns>
        [HttpGet]
        public IActionResult SyncFacebook(UserViewModel model)
        {
            try
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Success;
                
                var oResult = UserClient.Register(new UserModel(){
                    document_type = new DocumentTypeModel() { name = "Cedula" },
                    identification = model.identification,
                    name = model.name,
                    lastname = model.lastname,
                    celphone = model.celphone,
                    email = model.email
                });

                obResponse.DataResponse = new Models.UserViewModel(oResult);
                obResponse.Message = "Se sincronizó la cuenta correctamente!!";
            }
            catch (Exception ex)
            {
                obResponse.Status = (int)Enumerations.enumTypeMessageResponse.Danger;
                obResponse.Message = "Error al procesar la solicitud";
                obResponse.InnerException = ex.Message;
            }

            return Ok(obResponse);
        }
    }
}
