using System;
using Chismografo.Core.Models;

namespace Chismografo.Api.Models
{
    public class UserViewModel
    {
        public UserViewModel()
        {}

        public UserViewModel(UserModel model)
        {
            this.id_public = model.id_public;
            this.document_type_id = model.document_type != null ? model.document_type.Id.ToString() : null ;
            this.identification = model.identification;
            this.name = model.name;
            this.lastname = model.lastname;
            this.celphone = model.celphone;
            this.photo = model.photo;
            this.email = model.email;
            this.social_info = new SocialViewModel();
            this.status = (int)model.status;
            this.created_at = model.created_at;
            this.updated_at = model.updated_at;
        }

        public string id_public { get; set; }

        public string document_type_id { get; set; }

        public string identification { get; set; }

        public string name { get; set; }

        public string lastname { get; set; }

        public string celphone { get; set; }

        public string photo { get; set; }

        public string email { get; set; }

        public int status { get; set; }
        public SocialViewModel social_info { get; set; }

        public DateTime? created_at { get; set; }

        public DateTime? updated_at { get; set; }
    }
}