using MongoDB.Bson;
using System;
using Chismografo.Core.Models.Util;
using Chismografo.Core.Models;

namespace Chismografo.Api.Models
{
    public class CategoryQuestionViewModel
    {

        public CategoryQuestionViewModel()
        {}
        
        public CategoryQuestionViewModel(CategoryQuestionModel model)
        {
            this.id_public =  model.id_public; 
            this.name =  model.name; 
            this.status =  (int)model.status; 
            this.created_at =  model.created_at; 
            this.updated_at =  model.updated_at;   
        }
        public string id_public { get; set; }
        public string name { get; set; }
        public int status { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
    }
}