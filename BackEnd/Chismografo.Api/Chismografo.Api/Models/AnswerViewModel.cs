using MongoDB.Bson;
using System;
using Chismografo.Core.Models.Util;
using Chismografo.Core.Models;

namespace Chismografo.Api.Models
{
    public class AnswerViewModel
    {

        public AnswerViewModel()
        {}

        public AnswerViewModel(AnswerModel model)
        {
            this.id = model.Id.ToString();
            this.text = model.text;
            this.status = (int)model.status;
            this.user = new UserViewModel(model.user);
            this.created_at = model.created_at;
            this.updated_at = model.updated_at;
        }
        public string id { get; set; }
        public string text { get; set; }
        public int status { get; set; }
        public UserViewModel user { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }

        public string _id_public_question { get; set; }
        
    }
}