using MongoDB.Bson;
using Chismografo.Core.Models.Util;
using System;
using Chismografo.Core.Models;

namespace Chismografo.Api.Models
{
    public class DocumentTypeViewModel
    {
        private DocumentTypeModel _model { get; set; }
        public DocumentTypeViewModel(DocumentTypeModel model)
        {
            _model = model;
        }
        public ObjectId Id { get; set; }
        public string name { get; set; }
        public Enumerations.EnumGeneralStatus status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}