
using System;
using Chismografo.Core.Models;

namespace Chismografo.Api.Models
{
    public class InvitationViewModel
    {
        private InvitationModel _model { get; set; }
        public InvitationViewModel(InvitationModel model)
        {
            _model = model;
        }

        public int invitation_type { get{
            return _model != null ? (int)_model.invitation_type : 0;
        } set {} }
        public string invitation_link { get{
            return _model != null ? _model.invitation_link : null;
        } set {} }
        public int link_status { get{
            return _model != null ? (int)_model.link_status : 0;
        } set {} }
         public GroupViewModel group { get{
            return _model != null ? new GroupViewModel(_model.group) : null;
        } set {} }
        public UserViewModel guest { get{
            return _model != null ? new UserViewModel(_model.guest) : null;
        } set {} }
        public DateTime created_at { get{
            return _model != null ? _model.created_at : new DateTime();
        } set {} }
        public DateTime updated_at { get{
            return _model != null ? _model.updated_at : new DateTime();
        } set {} }
    }
}