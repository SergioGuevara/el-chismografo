using System.Collections.Generic;
using System.Linq;
using Chismografo.Core.Models;
using System;

namespace Chismografo.Api.Models
{
    public class GroupViewModel
    {
        public GroupViewModel()
        {}
        
        public GroupViewModel(GroupModel model)
        {
            this.id_public = model.id_public;
            this.name = model.name;
            this.members = new List<UserViewModel>();
            model.members?.All( x=> {
                this.members.Add(new UserViewModel(x));
                return true;
            });
            this.questions = new List<QuestionViewModel>();
            model.questions?.All( x=> {
                this.questions.Add(new QuestionViewModel(x));
                return true;
            });
            this.creator_user = new UserViewModel(model.creator_user);
            this.status = (int)model.status;
            this.created_at = model.created_at;
            this.updated_at = model.updated_at;
        }
        public string id_public { get; set; }
        public string name { get; set; }
        public List<UserViewModel> members { get; set; }
        public List<QuestionViewModel> questions { get; set; }
        public UserViewModel creator_user { get; set; }
        public int status { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
    }
}