using System.Collections.Generic;
using System;
using System.Linq;
using Chismografo.Core.Models;

namespace Chismografo.Api.Models
{
    public class QuestionViewModel
    {

        public QuestionViewModel()
        {}
        
        public QuestionViewModel(QuestionModel model)
        {
            this.id_public = model.id_public.ToString();
            this.question = model.question;
            this.category = new CategoryQuestionViewModel(model.category);
            this.answers = new List<AnswerViewModel>();
            /*model.answers?.All(x => {
                this.answers.Add(new AnswerViewModel(x));
                return true;
            });*/
            this.status = (int)model.status;
            this.created_at = model.created_at;
            this.updated_at = model.updated_at;
        }

        public string id_public  { get; set; }
        public string question { get; set; }
        public CategoryQuestionViewModel category { get; set; }
        public List<AnswerViewModel> answers { get; set; }
        public int? status { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }


        public string id_public_group { get; set; }
    }
}