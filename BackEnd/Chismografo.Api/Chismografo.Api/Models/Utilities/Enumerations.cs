namespace Chismografo.Api.Models.Utilities
{
    public class Enumerations
    {
        public enum enumTypeMessageResponse
        {
            Success = 2084001, 
            Danger = 2084002, 
            Warning = 2084003, 
            Primary = 2048004
        }
    }
}