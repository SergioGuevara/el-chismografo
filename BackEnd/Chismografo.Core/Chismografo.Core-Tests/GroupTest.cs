using Chismografo.Core.Client;
using Chismografo.Core.Models;
using Chismografo.Core.Models.Util;
using System.Collections.Generic;
using System;
using Xunit;
using MongoDB.Bson;

namespace Chismografo.Core_Tests
{
    public class GroupTest
    {
        [Fact]
        public void CreateGroupTest()
        {

            GroupModel n = new GroupModel(){
                name = "Grupo de prueba",
                members = new List<UserModel>(){
                    new UserModel(){ 
                        Id = new ObjectId("5f14a777c5b4cf246a36573b"),
                        id_public = Guid.NewGuid().ToString(),
                        document_type = new DocumentTypeModel() { name = "CC" },
                        identification = "111111",
                        name = "Carlos",
                        lastname = "Perez",
                        celphone = "313 482 5436",
                        email = "carlos@gmail.com",
                        status = (int)Enumerations.EnumGeneralStatus.Enabled,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                    }
                },
                questions = null,
                creator_user = new UserModel(){ 
                        id_public = Guid.NewGuid().ToString(),
                        document_type = new DocumentTypeModel() { name = "CC" },
                        identification = "3333",
                        name = "Juan",
                        lastname = "Ruiz",
                        celphone = "3134 482 5436",
                        email = "Juan@gmail.com",
                        status = (int)Enumerations.EnumGeneralStatus.Enabled,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                    },
                status = Enumerations.EnumGeneralStatus.Enabled,
                created_at = DateTime.Now,
                updated_at = DateTime.Now
            };
            var oReturn = GroupClient.CreateGroup(n);
        }
    }
}