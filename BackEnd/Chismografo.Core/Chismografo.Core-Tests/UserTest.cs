using System;
using Xunit;
using Chismografo.Core.Client;
using Chismografo.Core.Models;

namespace Chismografo.Core_Tests
{
    public class UserTest
    {
        [Fact]
        public void RegisterTest()
        {
            var oResponse = UserClient.Register(new UserModel(){ 
                        document_type = new DocumentTypeModel() { name = "CC" },
                        identification = "111111",
                        name = "Carlos",
                        lastname = "Perez",
                        celphone = "313 482 5436",
                        email = "carlos@gmail.com"
                    });
        }
    }
}
