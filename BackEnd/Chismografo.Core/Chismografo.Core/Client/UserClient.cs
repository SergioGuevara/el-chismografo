using System.Collections.Generic;
using Chismografo.Core.Models;
using Chismografo.Core.Interfaces;
using Chismografo.Core.Controller;
using Autofac;
using System;

namespace Chismografo.Core.Client
{
    public static class UserClient
    {

        /// <summary>
        /// Realiza el registro de un nuevo usuario
        /// </summary>
        /// <param name="user">Modelo de usuario para registrarlo</param>
        /// <returns>Modelo de usuario registrado</returns>
        public static UserModel Register(UserModel user){
            var builder = new ContainerBuilder();
            builder.RegisterType<UserController>().As<IUser>();

            var container = builder.Build();
            return container.Resolve<IUser>().Register(user);
        }

        /// <summary>
        /// Realiza el logueo de un usuario
        /// </summary>
        /// <param name="user">Modelo de usuario para registrarlo</param>
        /// <returns>Modelo de usuario registrado</returns>
        public static UserModel Login(UserModel user){
            var builder = new ContainerBuilder();
            builder.RegisterType<UserController>().As<IUser>();

            var container = builder.Build();
            return container.Resolve<IUser>().Login(user);
        }

        /// <summary>
        /// Realiza el registro de cierre de session
        /// </summary>
        /// <param name="user">Modelo de usuario para registrarlo</param>
        /// <returns>booleano</returns>
        public static Boolean Logout(UserModel user){
            var builder = new ContainerBuilder();
            builder.RegisterType<UserController>().As<IUser>();

            var container = builder.Build();
            return container.Resolve<IUser>().Logout(user);
        }

        /// <summary>
        /// Actualización de la información de registro
        /// </summary>
        /// <param name="user">Modelo de usuario para actualizar</param>
        /// <returns>Modelo de usuario actualizado</returns>
        public static UserModel UpdateProfile(UserModel user){
            var builder = new ContainerBuilder();
            builder.RegisterType<UserController>().As<IUser>();

            var container = builder.Build();
            return container.Resolve<IUser>().UpdateProfile(user);
        }

        /// <summary>
        /// Busca a usuario activos
        /// </summary>
        /// <param name="pageNumber">Numero de pagina</param>
        /// <param name="pageSize">Tamaño de Pagina</param>
        /// <param name="search">Parametro de Busqueda</param>
        /// <returns>Conteo de resultados y Listado de usuarios activos encontrados</returns>
        public static Tuple<int, List<UserModel>> SearchUsers(int? pageNumber, int? pageSize, string search){
            var builder = new ContainerBuilder();
            builder.RegisterType<UserController>().As<IUser>();

            var container = builder.Build();
            return container.Resolve<IUser>().SearchUsers(pageNumber, pageSize, search);
        }

        /// <summary>
        /// Retorna las notificacion sin leer (APP) para un usuario en especifico
        /// </summary>
        /// <param name="element">Modelo de Usuario en session</param>
        /// <returns>Lista de notificaciones</returns>
        public static List<NotificationServices.Models.NotificationModel> GetNotification(UserModel element){
            var builder = new ContainerBuilder();
            builder.RegisterType<UserController>().As<IUser>();

            var container = builder.Build();
            return container.Resolve<IUser>().GetNotification(element);
        }

        /// <summary>
        /// Sincroniza el usuario con una cuenta de Facebook
        /// </summary>
        /// <returns></returns>
        public static bool SyncFacebook(){
            var builder = new ContainerBuilder();
            builder.RegisterType<UserController>().As<IUser>();

            var container = builder.Build();
            return container.Resolve<IUser>().SyncFacebook();
        }
    }
}