using Chismografo.Core.Models.Util;
using Chismografo.Core.Models;
using Chismografo.Core.Interfaces;
using Chismografo.Core.Controller;
using Autofac;
using System.Collections.Generic;

namespace Chismografo.Core.Client
{
    public static class QuestionClient
    {

        /// <summary>
        /// Crear una pregunta
        /// </summary>
        /// <param name="question">Modelo de la pregunta para crear</param>
        /// <returns>Modelo de la pregunta creada</returns>
        public static QuestionModel CreateQuestion(QuestionModel question){
            var builder = new ContainerBuilder();
            builder.RegisterType<QuestionController>().As<IQuestion>();

            var container = builder.Build();
            return container.Resolve<IQuestion>().CreateQuestion(question);
        }

        /// <summary>
        /// Modifica una pregunta
        /// </summary>
        /// <param name="question">Modelo de la pregunta a modificar</param>
        /// <returns>Modelo de la pregunta modificadá</returns>
        public static QuestionModel EditQuestion(QuestionModel question){
            var builder = new ContainerBuilder();
            builder.RegisterType<QuestionController>().As<IQuestion>();

            var container = builder.Build();
            return container.Resolve<IQuestion>().EditQuestion(question);
        }

        /// <summary>
        /// Obtiene una pregunta
        /// </summary>
        /// <param name="id_public">Identificador publico de la pregunta</param>
        /// <returns>Modelo de la pregunta a obtener</returns>
        public static QuestionModel GetQuestion(string id_public){
            var builder = new ContainerBuilder();
            builder.RegisterType<QuestionController>().As<IQuestion>();

            var container = builder.Build();
            return container.Resolve<IQuestion>().GetQuestion(id_public);
        }

        /// <summary>
        /// Lista todas las preguntas, que no esten des habilitadas
        /// </summary>
        /// <returns>listado de preguntas (QuestionModel)</returns>
        public static List<QuestionModel> getEnabledQuestions(){
            var builder = new ContainerBuilder();
            builder.RegisterType<QuestionController>().As<IQuestion>();

            var container = builder.Build();
            return container.Resolve<IQuestion>().getEnabledQuestions();
        }

        /// <summary>
        /// Expone el cambio de estado de una pregunta
        /// </summary>
        /// <param name="id">id_public de la pregunta</param>
        /// <returns>La pregunta afectada(QuestionViewModel)</returns>
        public static QuestionModel changeStatus(string id_public){
            var builder = new ContainerBuilder();
            builder.RegisterType<QuestionController>().As<IQuestion>();

            var container = builder.Build();
            return container.Resolve<IQuestion>().changeStatus(id_public);
        }
        
    }
}