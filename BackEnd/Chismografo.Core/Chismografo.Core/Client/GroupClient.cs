using Chismografo.Core.Models.Util;
using Chismografo.Core.Models;
using Chismografo.Core.Interfaces;
using Chismografo.Core.Controller;
using Autofac;
using System.Collections.Generic;

namespace Chismografo.Core.Client
{
    public static class GroupClient
    {

        #region Group Methods

        /// <summary>
        /// Crear un grupo para un usuario determinado y si tiene listado de invitados envia la invitación
        /// </summary>
        /// <param name="group">Modelo de grupo ha crear</param>
        /// <returns>Modelo de grupo creado</returns>
        public static GroupModel CreateGroup(GroupModel group){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().CreateGroup(group);
        }

        /// <summary>
        /// Edita la info de un grupo 
        /// </summary>
        /// <param name="id_user">Identificación publica del usuario logeado</param>
        /// <param name="group">Modelo de grupo ha modificar</param>
        /// <returns>Modelo de grupo creado</returns>
        public static GroupModel EditGroup(string id_user, GroupModel group){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().EditGroup(id_user, group);
        }

        /// <summary>
        /// Elimina un grupo
        /// </summary>
        /// <param name="id_user">Identificación publica del usuario logeado</param>
        /// <param name="id_public">Identificación publica del grupo</param>
        public static void DeleteGroup(string id_user, string id_public){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            container.Resolve<IGroup>().DeleteGroup(id_user, id_public);
        }

        /// <summary>
        /// Salida de un miembre de un grupo
        /// </summary>
        /// <param name="id_user">id del usuario</param>
        /// <param name="id_group">id del grupo</param>
        /// <returns>Valor Bool para identificar si la tarea se hizo correctamente</returns>
        public static GroupModel LeaveGroup(string id_user, string id_group){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().LeaveGroup(id_user, id_group);
        }

        /// <summary>
        /// Obtine un grupo
        /// </summary>
        /// <param name="id_public">id publico del grupo</param>
        /// <returns>Modelo del grupo encontrado</returns>
        public static GroupModel GetGroup(string id_public){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().GetGroup(id_public);
        }

        /// <summary>
        /// Obtiene los grupos de un usuario
        /// </summary>
        /// <param name="id_public">id publico del usuario</param>
        /// <returns>Listado de modelos del grupos de un usuario</returns>
        public static List<GroupModel> GetGroupsOfUser(string id_public){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().GetGroupsOfUser(id_public);
        }

        /// <summary>
        /// Envia la invitación a usuarios para unirsen a un grupo (App/Email/Facebook)
        /// </summary>
        /// <param name="group">Modelo de grupo al cual se realizará la invitación</param>
        /// <returns>Valor Bool para identificar si la tarea se hizo correctamente</returns>
        public static bool SendInvitation(string id_group, string id_user, Enumerations.EnumInvitationType invitation_type){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().SendInvitation(id_group,id_user, invitation_type);
        }

        /// <summary>
        /// Genera link de invitación a un grupo
        /// </summary>
        /// <param name="id_group">Id publico del grupo</param>
        /// <returns>Link de invitación</returns>
        public static string GetInvitationLink(string id_group){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().GetInvitationLink(id_group);
        }
    
        #endregion

        #region Question 

        /// <summary>
        /// Crea una pregunta dentro de un grupo
        /// </summary>
        /// <param name="id_group">Identificación del grupo</param>
        /// <param name="id_question">Modelo de la pregunta que se va ha agregar al grupo</param>
        /// <returns>Modelo del grupo afectado</returns>
        public static GroupModel CreateQuestion(string id_group, QuestionModel question){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().CreateQuestion(id_group, question);
        }

        /// <summary>
        /// Modifica una pregunta dentro de un grupo
        /// </summary>
        /// <param name="question">Modelo de la pregunta que se va ha modificar</param>
        /// <returns>Modelo del grupo afectado</returns>
        public static GroupModel EditQuestion(QuestionModel question){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().EditQuestion(question);
        }

        /// <summary>
        /// Elimina una pregunta dentro de un grupo
        /// </summary>
        /// <param name="id_user">Identificación publica del usuario logeado</param>
        /// <param name="id_question">Identificación publica de una pregunta que se va ha elimnar del grupo</param>
        /// <returns>Modelo del grupo afectado</returns>
        public static void DeleteQuestion(string id_user, string id_question){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            container.Resolve<IGroup>().DeleteQuestion(id_user, id_question);
        }

        /// <summary>
        /// Obtine un modelo de una pregunta, dentro de un grupo
        /// </summary>
        /// <param name="id_public">Identificador publico de la pregunta</param>
        /// <returns>Modelo de la pregunta encontrada</returns>
        public static QuestionModel GetQuestion(string id_public){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().GetQuestion(id_public);
        }

        #endregion

        #region Answer

        /// <summary>
        /// Responde una pregunta de un grupo
        /// </summary>
        /// <param name="id_question">Identificación publica de la pregunta</param>
        /// <param name="id_user">Identificación publica del usuario que esta dando la respuesta</param>
        /// <param name="answer">Modelo de la respuesta</param>
        /// <returns>Modelo del grupo afectado</returns>
        public static GroupModel AnswerQuestion(string id_question, string id_user, AnswerModel answer){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().AnswerQuestion(id_question, id_user, answer);
        }

        /// <summary>
        /// Eliminar una resputa
        /// </summary>
        /// <param name="id_answer">Identificación de la respuesta</param>
        /// <param name="id_user">Identificación publica del usuario se encuentra logeoado</param>
        /// <returns>Modelo del grupo afectado</returns>
        public static GroupModel DeleteAnswer(string id_answer, string id_user){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().DeleteAnswer(id_answer, id_user);  
        }

        /// <summary>
        /// Editar una resputa
        /// </summary>
        /// <param name="id_user">Identificación publica del usuario se encuentra logeoado</param>
        /// <param name="answer">Modelo de la respuesta</param>
        /// <returns>Modelo del grupo afectado</returns>
        public static GroupModel EditAnswer(string id_user, AnswerModel answer){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().EditAnswer(id_user, answer);  
        }

        /// <summary>
        /// Consulta el listado de respuestas para una pregunta
        /// </summary>
        /// <param name="id_question">Identificación de la pregunta</param>
        /// <returns>Listado de resputas </returns>
        public static List<AnswerModel> GetAnswers(string id_question){
            var builder = new ContainerBuilder();
            builder.RegisterType<GroupController>().As<IGroup>();

            var container = builder.Build();
            return container.Resolve<IGroup>().GetAnswers(id_question);  
        }

        #endregion 
    }
}