using Chismografo.Core.Models.Util;
using Chismografo.Core.Models;
using Chismografo.Core.Interfaces;
using Chismografo.Core.Controller;
using Autofac;
using System.Collections.Generic;

namespace Chismografo.Core.Client
{
    public static class CategoryQuestionClient
    {

        /// <summary>
        /// Crear una categoria de preguntas
        /// </summary>
        /// <param name="category">Modelo de una categoria a crear</param>
        /// <returns>Modelo de la categoria creada</returns>
        public static CategoryQuestionModel CreateCategoryQuestion(CategoryQuestionModel category){
            var builder = new ContainerBuilder();
            builder.RegisterType<CategoryQuestionController>().As<ICategoryQuestion>();

            var container = builder.Build();
            return container.Resolve<ICategoryQuestion>().CreateCategoryQuestion(category);
        }

        /// <summary>
        /// Modifica una categoria de preguntas
        /// </summary>
        /// <param name="category">Modelo de una categoria a modificar</param>
        /// <returns>Modelo de la categoria modificadá</returns>
        public static CategoryQuestionModel EditCategoryQuestion(CategoryQuestionModel category){
            var builder = new ContainerBuilder();
            builder.RegisterType<CategoryQuestionController>().As<ICategoryQuestion>();

            var container = builder.Build();
            return container.Resolve<ICategoryQuestion>().EditCategoryQuestion(category);
        }

        /// <summary>
        /// Obtiene una categoria de preguntas
        /// </summary>
        /// <param name="id_public">Identificador publico de la categoria</param>
        /// <returns>Modelo de la categoria a obtener</returns>
        public static CategoryQuestionModel GetCategoryQuestion(string id_public){
            var builder = new ContainerBuilder();
            builder.RegisterType<CategoryQuestionController>().As<ICategoryQuestion>();

            var container = builder.Build();
            return container.Resolve<ICategoryQuestion>().GetCategoryQuestion(id_public);       
        }

        /// <summary>
        /// Lista todas las categorias existentes, incluso las deshabilitadas
        /// </summary>
        /// <returns>Listado de categorias (CategoryQuestionModel)</returns>
        public static List<CategoryQuestionModel> getAll(){
            var builder = new ContainerBuilder();
            builder.RegisterType<CategoryQuestionController>().As<ICategoryQuestion>();

            var container = builder.Build();
            return container.Resolve<ICategoryQuestion>().getAll();
        }

        /// <summary>
        /// Cambia de estado de una categoria
        /// </summary>
        /// <param name="id">id_public de la categoria</param>
        /// <returns>La categoria afectada(CategoryQuestionModel)</returns>
        public static CategoryQuestionModel changeStatus(string id_public)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<CategoryQuestionController>().As<ICategoryQuestion>();

            var container = builder.Build();
            return container.Resolve<ICategoryQuestion>().changeStatus(id_public);
        }

    }
}