using System;
using Chismografo.Core.Models;
using System.Collections.Generic;

namespace Chismografo.Core.Interfaces
{
    public interface ICategoryQuestion
    {

        /// <summary>
        /// Crear una categoria de preguntas
        /// </summary>
        /// <param name="category">Modelo de una categoria a crear</param>
        /// <returns>Modelo de la categoria creada</returns>
        CategoryQuestionModel CreateCategoryQuestion(CategoryQuestionModel category);

        /// <summary>
        /// Modifica una categoria de preguntas
        /// </summary>
        /// <param name="category">Modelo de una categoria a modificar</param>
        /// <returns>Modelo de la categoria modificadá</returns>
        CategoryQuestionModel EditCategoryQuestion(CategoryQuestionModel category);

        /// <summary>
        /// Obtiene una categoria de preguntas
        /// </summary>
        /// <param name="id_public">Identificador publico de la categoria</param>
        /// <returns>Modelo de la categoria a obtener</returns>
        CategoryQuestionModel GetCategoryQuestion(string id_public);

        /// <summary>
        /// Lista todas las categorias existentes, incluso las deshabilitadas
        /// </summary>
        /// <returns>Listado de categorias (CategoryQuestionModel)</returns>
        List<CategoryQuestionModel> getAll();

        /// <summary>
        /// Cambia de estado de una categoria
        /// </summary>
        /// <param name="id">id_public de la categoria</param>
        /// <returns>La categoria afectada(CategoryQuestionModel)</returns>
        CategoryQuestionModel changeStatus(string id);

    }
}