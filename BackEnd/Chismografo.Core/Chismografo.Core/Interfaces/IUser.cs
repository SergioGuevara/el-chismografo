using Chismografo.Core.Models;
using System.Collections.Generic;
using System;

namespace Chismografo.Core.Interfaces
{
    public interface IUser
    {

        /// <summary>
        /// Realiza el registro de un nuevo usuario
        /// </summary>
        /// <param name="user">Modelo de usuario para registrarlo</param>
        /// <returns>Modelo de usuario registrado</returns>
        UserModel Register(UserModel user);

        /// <summary>
        /// Realiza el logueo de un usuario
        /// </summary>
        /// <param name="user">Modelo de usuario para registrarlo</param>
        /// <returns>Modelo de usuario registrado</returns>
        UserModel Login(UserModel user);

        /// <summary>
        /// Realiza el registro de cierre de session
        /// </summary>
        /// <param name="user">Modelo de usuario para registrarlo</param>
        /// <returns>booleano</returns>
        Boolean Logout(UserModel user);

        /// <summary>
        /// Actualización de la información de registro
        /// </summary>
        /// <param name="user">Modelo de usuario para actualizar</param>
        /// <returns>Modelo de usuario actualizado</returns>
        UserModel UpdateProfile(UserModel user);

        /// <summary>
        /// Busca a usuario activos
        /// </summary>
        /// <param name="pageNumber">Numero de pagina</param>
        /// <param name="pageSize">Tamaño de Pagina</param>
        /// <param name="search">Parametro de Busqueda</param>
        /// <returns>Conteo de resultados y Listado de usuarios activos encontrados</returns>
        Tuple<int, List<UserModel>> SearchUsers(int? pageNumber, int? pageSize, string search);

        /// <summary>
        /// Retorna las notificacion sin leer (APP) para un usuario en especifico
        /// </summary>
        /// <param name="element">Modelo de Usuario en session</param>
        /// <returns>Lista de notificaciones</returns>
        List<NotificationServices.Models.NotificationModel> GetNotification(UserModel element);

        /// <summary>
        /// Sincroniza el usuario con una cuenta de Facebook
        /// </summary>
        /// <returns></returns>
        bool SyncFacebook();
    }
}