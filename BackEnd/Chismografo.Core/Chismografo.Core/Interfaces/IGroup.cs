using Chismografo.Core.Models;
using Chismografo.Core.Models.Util;
using System.Collections.Generic;

namespace Chismografo.Core.Interfaces
{
    public interface IGroup
    {
        #region Group Methods

        /// <summary>
        /// Crear un grupo para un usuario determinado y si tiene listado de invitados envia la invitación
        /// </summary>
        /// <param name="group">Modelo de grupo ha crear</param>
        /// <returns>Modelo de grupo creado</returns>
        GroupModel CreateGroup(GroupModel group);

        /// <summary>
        /// Edita la info de un grupo 
        /// </summary>
        /// <param name="id_user">Identificación publica del usuario logeado</param>
        /// <param name="group">Modelo de grupo ha modificar</param>
        /// <returns>Modelo de grupo creado</returns>
        GroupModel EditGroup(string id_user, GroupModel group);

        /// <summary>
        /// Elimina un grupo
        /// </summary>
        /// <param name="id_user">Identificación publica del usuario logeado</param>
        /// <param name="id_public">Identificación publica del grupo</param>
        void DeleteGroup(string id_user, string id_public);

        /// <summary>
        /// Salida de un miembre de un grupo
        /// </summary>
        /// <param name="id_user">id del usuario</param>
        /// <param name="id_group">id del grupo</param>
        /// <returns>Valor Bool para identificar si la tarea se hizo correctamente</returns>
        GroupModel LeaveGroup(string id_user, string id_group);

        /// <summary>
        /// Obtine un grupo
        /// </summary>
        /// <param name="id_public">id publico del grupo</param>
        /// <returns>Modelo del grupo encontrado</returns>
        GroupModel GetGroup(string id_public);

        /// <summary>
        /// Obtiene los grupos de un usuario
        /// </summary>
        /// <param name="id_public">id publico del usuario</param>
        /// <returns>Listado de modelos del grupos de un usuario</returns>
        List<GroupModel> GetGroupsOfUser(string id_public);

        /// <summary>
        /// Envia la invitación a usuarios para unirsen a un grupo (App/Email/Facebook)
        /// </summary>
        /// <param name="group">Modelo de grupo al cual se realizará la invitación</param>
        /// <returns>Valor Bool para identificar si la tarea se hizo correctamente</returns>
        bool SendInvitation(string id_group, string id_user, Enumerations.EnumInvitationType invitation_type);

        /// <summary>
        /// Genera link de invitación a un grupo
        /// </summary>
        /// <param name="id_group">Id publico del grupo</param>
        /// <returns>Link de invitación</returns>
        string GetInvitationLink(string id_group);

        #endregion
        
        #region Question Methods 

        /// <summary>
        /// Crea una pregunta dentro de un grupo
        /// </summary>
        /// <param name="id_public">Identificación del grupo</param>
        /// <param name="question">Modelo de la pregunta que se va ha agregar al grupo</param>
        /// <returns>Modelo del grupo afectado</returns>
        GroupModel CreateQuestion(string id_public, QuestionModel question);

        /// <summary>
        /// Modifica una pregunta dentro de un grupo
        /// </summary>
        /// <param name="question">Modelo de la pregunta que se va ha modificar</param>
        /// <returns>Modelo del grupo afectado</returns>
        GroupModel EditQuestion(QuestionModel question);

        /// <summary>
        /// Elimina una pregunta dentro de un grupo
        /// </summary>
        /// <param name="id_user">Identificación publica del usuario logeado</param>
        /// <param name="id_question">Identificación publica de una pregunta que se va ha elimnar del grupo</param>
        /// <returns>Modelo del grupo afectado</returns>
        void DeleteQuestion(string id_user, string id_question);

        /// <summary>
        /// Obtine un modelo de una pregunta, dentro de un grupo
        /// </summary>
        /// <param name="id_public">Identificador publico de la pregunta</param>
        /// <returns>Modelo de la pregunta encontrada</returns>
        QuestionModel GetQuestion(string id_public);

        #endregion

        #region Answer

        /// <summary>
        /// Responde una pregunta de un grupo
        /// </summary>
        /// <param name="id_question">Identificación publica de la pregunta</param>
        /// <param name="id_user">Identificación publica del usuario que esta dando la respuesta</param>
        /// <param name="answer">Modelo de la respuesta</param>
        /// <returns>Modelo del grupo afectado</returns>
        GroupModel AnswerQuestion(string id_question, string id_user, AnswerModel answer);

        /// <summary>
        /// Eliminar una resputa
        /// </summary>
        /// <param name="id_answer">Identificación de la respuesta</param>
        /// <param name="id_user">Identificación publica del usuario se encuentra logeoado</param>
        /// <returns>Modelo del grupo afectado</returns>
        GroupModel DeleteAnswer(string id_answer, string id_user);

        /// <summary>
        /// Editar una resputa
        /// </summary>
        /// <param name="id_user">Identificación publica del usuario se encuentra logeoado</param>
        /// <param name="answer">Modelo de la respuesta</param>
        /// <returns>Modelo del grupo afectado</returns>
        GroupModel EditAnswer(string id_user, AnswerModel answer);

        /// <summary>
        /// Consulta el listado de respuestas para una pregunta
        /// </summary>
        /// <param name="id_question">Identificación de la pregunta</param>
        /// <returns>Listado de resputas </returns>
        List<AnswerModel> GetAnswers(string id_question);

        #endregion
    }
}