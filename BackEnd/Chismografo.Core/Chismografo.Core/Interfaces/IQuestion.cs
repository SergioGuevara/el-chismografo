using System.Collections.Generic;
using Chismografo.Core.Models;

namespace Chismografo.Core.Interfaces
{
    public interface IQuestion
    {
        /// <summary>
        /// Crear una pregunta
        /// </summary>
        /// <param name="question">Modelo de la pregunta para crear</param>
        /// <returns>Modelo de la pregunta creada</returns>
        QuestionModel CreateQuestion(QuestionModel question);

        /// <summary>
        /// Modifica una pregunta
        /// </summary>
        /// <param name="question">Modelo de la pregunta a modificar</param>
        /// <returns>Modelo de la pregunta modificadá</returns>
        QuestionModel EditQuestion(QuestionModel question);

        /// <summary>
        /// Obtiene una pregunta
        /// </summary>
        /// <param name="id_public">Identificador publico de la pregunta</param>
        /// <returns>Modelo de la pregunta a obtener</returns>
        QuestionModel GetQuestion(string id_public);

        /// <summary>
        /// Lista todas las preguntas, que no esten des habilitadas
        /// </summary>
        /// <returns>listado de preguntas (QuestionModel)</returns>
        List<QuestionModel> getEnabledQuestions();

        /// <summary>
        /// Expone el cambio de estado de una pregunta
        /// </summary>
        /// <param name="id">id_public de la pregunta</param>
        /// <returns>La pregunta afectada(QuestionViewModel)</returns>
        QuestionModel changeStatus(string id);
    
    }
}