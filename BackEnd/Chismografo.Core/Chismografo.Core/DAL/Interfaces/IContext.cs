using Chismografo.Core.Models;
using Chismografo.Core.DAL.Interfaces;

namespace Chismografo.Core.DAL.Interfaces
{
    public interface IContext
    {
        IADO<UserModel> Users { get; set; }
        IADO<QuestionModel> Questions { get; set; }
        IADO<InvitationModel> Invitations { get; set; }
        IADO<GroupModel> Groups { get; set; }
        IADO<DocumentTypeModel> DocumentTypes { get; set; }
        IADO<CategoryQuestionModel> CategoryQuestions { get; set; }

    }
}