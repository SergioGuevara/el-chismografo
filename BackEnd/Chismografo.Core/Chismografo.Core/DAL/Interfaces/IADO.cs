using System.Collections.Generic;

namespace Chismografo.Core.DAL.Interfaces
{
    public interface IADO<T>
    {

        List<T> ToList();

        T Find(string Id);
    
        void Update(T entity);

        string Save(T entity);
    }
}