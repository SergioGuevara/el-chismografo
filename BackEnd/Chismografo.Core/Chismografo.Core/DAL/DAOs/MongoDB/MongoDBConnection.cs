using Chismografo.Core.DAL.Interfaces;
using MongoDB.Driver;
using Chismografo.Core.Models.Util;

namespace Chismografo.Core.DAL.DAOs.MongoDB
{
    internal class MongoDBConnection : IConnection<IMongoDatabase>
    {
        public IMongoDatabase _database { get; set; }

        public MongoDBConnection()
        {
            //"mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false"
            var client = new MongoClient("mongodb://127.0.0.1:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false");
            //_database = client.GetDatabase(Constants.C_LS_ProviderConnectionName); //"chismografo_log"
            _database = client.GetDatabase("chismografo"); 
        }

    }
}