using Chismografo.Core.DAL.Interfaces;
using Chismografo.Core.Models;
using MongoDB.Driver;
using MongoDB.Bson;

namespace Chismografo.Core.DAL.DAOs.MongoDB
{
    internal class MongoDBContext : IContext
    {
        public MongoDBContext()
        {
        }

        public IConnection<MongoDBConnection> _connection { get; set; }

        public IADO<UserModel> Users { get{
            return new MongoDB_ADO<UserModel>("user");
        } set {} }

        public IADO<QuestionModel> Questions { get{
            return new MongoDB_ADO<QuestionModel>("question");
        } set {} }

        public IADO<InvitationModel> Invitations { get{
            return new MongoDB_ADO<InvitationModel>("invitation");
        } set {} }

        public IADO<GroupModel> Groups { get{
            return new MongoDB_ADO<GroupModel>("group");
        } set {} }

        public IADO<DocumentTypeModel> DocumentTypes { get{
            return new MongoDB_ADO<DocumentTypeModel>("document_type");
        } set {} }

        public IADO<CategoryQuestionModel> CategoryQuestions { get{
            return new MongoDB_ADO<CategoryQuestionModel>("category_question");
        } set {} }
    }
}