using MongoDB.Bson;

namespace Chismografo.Core.DAL.DAOs.MongoDB.Interfaces
{
    internal interface IModel
    {
        ObjectId Id { get; set; }
    }
}