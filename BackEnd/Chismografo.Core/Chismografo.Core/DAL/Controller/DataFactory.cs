using System;
using Chismografo.Core.Models.Util;

namespace Chismografo.Core.DAL.Controller
{
    public class DataFactory
    {
        public Chismografo.Core.DAL.Interfaces.IContext GetDataInstance(){
            //Type typetoreturn = Type.GetType(Constants.C_LS_TypeProviderName);//"Chismografo.Core.DAL.DAOs.MongoDB.MongoDBContext,Context"
            Type typetoreturn = Type.GetType("Chismografo.Core.DAL.DAOs.MongoDB.MongoDBContext,Chismografo.Core");
            Chismografo.Core.DAL.Interfaces.IContext oReturn = (Chismografo.Core.DAL.Interfaces.IContext)Activator.CreateInstance(typetoreturn);
            return oReturn;
        }
    }
}