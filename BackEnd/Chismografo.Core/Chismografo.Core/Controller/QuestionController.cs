using Chismografo.Core.Interfaces;
using Chismografo.Core.DAL.Controller;
using Chismografo.Core.DAL.Interfaces;
using Chismografo.Core.Models;
using Chismografo.Core.Models.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Chismografo.Core.Controller
{
    public class QuestionController : IQuestion
    {
        public IContext _context { get; set; }

        public QuestionController(){
            if(_context == null){
                DataFactory factory = new DataFactory();
                _context = factory.GetDataInstance();
            }
        }

        /// <summary>
        /// Crear una pregunta
        /// </summary>
        /// <param name="question">Modelo de la pregunta para crear</param>
        /// <returns>Modelo de la pregunta creada</returns>
        public QuestionModel CreateQuestion(QuestionModel question){
            List<CategoryQuestionModel> categories = _context.CategoryQuestions.ToList();
            if(categories.Any(x => x.id_public == question.category.id_public)){
                question.category = categories.First(x=> x.id_public == question.category.id_public);
                if(question.category.status == Enumerations.EnumGeneralStatus.Disabled)
                    throw new ArgumentException("La categoria seleccionada se encuentra deshabilitada.");
                question.id_public = System.Guid.NewGuid().ToString();
                question.status = Enumerations.EnumGeneralStatus.Enabled;
                question.created_at = DateTime.Now;
                string id =_context.Questions.Save(question);
                return _context.Questions.Find(id);
            }else{
                throw new ArgumentException("La categoria no se encontró");
            }
        }

        /// <summary>
        /// Modifica una pregunta
        /// </summary>
        /// <param name="question">Modelo de la pregunta a modificar</param>
        /// <returns>Modelo de la pregunta modificadá</returns>
        public QuestionModel EditQuestion(QuestionModel question){
            QuestionModel questionEdit = _context.Questions.ToList().First(x => x.id_public == question.id_public);
            questionEdit.question = question.question;
            questionEdit.category = _context.CategoryQuestions.ToList().First(x=> x.id_public == question.category.id_public);
            questionEdit.updated_at = DateTime.Now;
            _context.Questions.Update(questionEdit);
            return question;
        }

        /// <summary>
        /// Obtiene una pregunta
        /// </summary>
        /// <param name="id_public">Identificador publico de la pregunta</param>
        /// <returns>Modelo de la pregunta a obtener</returns>
        public QuestionModel GetQuestion(string id_public){
            List<QuestionModel> questions = _context.Questions.ToList();
            if(questions.Any(x => x.id_public == id_public)){
                return questions.First(x => x.id_public == id_public);
            }else{
                throw new ArgumentException("No se encontro la pregunta.");
            }
        }

        /// <summary>
        /// Lista todas las preguntas, que no esten des habilitadas
        /// </summary>
        /// <returns>listado de preguntas (QuestionModel)</returns>
        public List<QuestionModel> getEnabledQuestions(){
            var questions = _context.Questions.ToList();
            return questions.Where(x => x.status == Enumerations.EnumGeneralStatus.Enabled ).ToList(); 
        }

        /// <summary>
        /// Expone el cambio de estado de una pregunta
        /// </summary>
        /// <param name="id">id_public de la pregunta</param>
        /// <returns>La pregunta afectada(QuestionViewModel)</returns>
        public QuestionModel changeStatus(string id_public){
            List<QuestionModel> questions = _context.Questions.ToList();
            if(questions.Any(x => x.id_public == id_public)){
                QuestionModel question = questions.First(x => x.id_public == id_public);
                question.status = question.status == Enumerations.EnumGeneralStatus.Disabled ? Enumerations.EnumGeneralStatus.Enabled : Enumerations.EnumGeneralStatus.Disabled; 
                question.updated_at = DateTime.Now;
                _context.Questions.Update(question);
                return question;
            }else{
                throw new ArgumentException("No se encontro la pregunta.");
            }
        }

    }
}