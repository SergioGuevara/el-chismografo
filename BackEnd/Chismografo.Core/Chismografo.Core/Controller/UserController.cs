using Chismografo.Core.Interfaces;
using Chismografo.Core.DAL.Controller;
using Chismografo.Core.DAL.Interfaces;
using System.Collections.Generic;
using Chismografo.Core.Models;
using Chismografo.Core.Models.Util;
using System;
using System.Linq;

namespace Chismografo.Core.Controller
{
    internal class UserController : IUser
    {
        public IContext _context { get; set; }

        public UserController()
        {
            if(_context == null){
                DataFactory factory = new DataFactory();
                _context = factory.GetDataInstance();
            }
        }

        /// <summary>
        /// Realiza el registro de un nuevo usuario
        /// </summary>
        /// <param name="user">Modelo de usuario para registrarlo</param>
        /// <returns>Modelo de usuario registrado</returns>
        public UserModel Register(UserModel user){
            List<UserModel> _users = _context.Users.ToList();

            if(_users.Any(x=> x.social_info.id == user.social_info.id)){
                throw new Exception("Ya se encuentra registrado el usuario!!");
            }
            user.id_public = Guid.NewGuid().ToString();
            user.status = (int)Enumerations.EnumGeneralStatus.Enabled;
            user.created_at = DateTime.Now;
            user.updated_at = DateTime.Now;
            string id = _context.Users.Save(user);
            return _context.Users.Find(id);
        }

        /// <summary>
        /// Realiza el logueo de un usuario
        /// </summary>
        /// <param name="user">Modelo de usuario para registrarlo</param>
        /// <returns>Modelo de usuario registrado</returns>
        public UserModel Login(UserModel user){
            List<UserModel> _users = _context.Users.ToList();

            if(_users.Any(x=> x.social_info.id == user.social_info.id)){
                return _users.FirstOrDefault(x=> x.social_info.id == user.social_info.id);
            }
            throw new Exception("No se encontro el usuario, por favor comuniquese con el administrador del sistema.");
        }

        /// <summary>
        /// Realiza el registro de cierre de session
        /// </summary>
        /// <param name="user">Modelo de usuario para registrarlo</param>
        /// <returns>booleano</returns>
        public Boolean Logout(UserModel user){
            List<UserModel> _users = _context.Users.ToList();

            if(_users.Any(x=> x.social_info.id == user.social_info.id)){
                return _users.Any(x=> x.social_info.id == user.social_info.id);
            }
            throw new Exception("No se encontro el usuario, por favor comuniquese con el administrador del sistema.");
        }

        /// <summary>
        /// Actualización de la información de registro
        /// </summary>
        /// <param name="user">Modelo de usuario para actualizar</param>
        /// <returns>Modelo de usuario actualizado</returns>
        public UserModel UpdateProfile(UserModel user){
            user.updated_at = DateTime.Now;
            _context.Users.Update(user);
            return _context.Users.Find(user.Id.ToString());;
        }

        /// <summary>
        /// Busca a usuario activos
        /// </summary>
        /// <param name="pageNumber">Numero de pagina</param>
        /// <param name="pageSize">Tamaño de Pagina</param>
        /// <param name="search">Parametro de Busqueda</param>
        /// <returns>Conteo de resultados y Listado de usuarios activos encontrados</returns>
        public Tuple<int, List<UserModel>> SearchUsers(int? pageNumber, int? pageSize, string search){
            Tuple<int, List<UserModel>> oReturn = new Tuple<int, List<UserModel>>(1, _context.Users.ToList());

            //_context.Users
            return oReturn;
        }

        /// <summary>
        /// Retorna las notificacion sin leer (APP) para un usuario en especifico
        /// </summary>
        /// <param name="element">Modelo de Usuario en session</param>
        /// <returns>Lista de notificaciones</returns>
        public List<NotificationServices.Models.NotificationModel> GetNotification(UserModel element){
            return NotificationServices.Client.NotificationClient.GetNotificationByUser(element.id_public.ToString());
        }

        /// <summary>
        /// Sincroniza el usuario con una cuenta de Facebook
        /// </summary>
        /// <returns></returns>
        public bool SyncFacebook(){
            return true;
        }
    }
}