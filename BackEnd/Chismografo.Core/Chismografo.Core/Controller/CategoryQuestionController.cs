using Chismografo.Core.Interfaces;
using Chismografo.Core.DAL.Controller;
using Chismografo.Core.DAL.Interfaces;
using Chismografo.Core.Models;
using Chismografo.Core.Models.Util;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Chismografo.Core.Controller
{
    public class CategoryQuestionController : ICategoryQuestion
    {
        public IContext _context { get; set; }

        public CategoryQuestionController(){
            if(_context == null){
                DataFactory factory = new DataFactory();
                _context = factory.GetDataInstance();
            }
        }

        /// <summary>
        /// Crear una categoria de preguntas
        /// </summary>
        /// <param name="category">Modelo de una categoria a crear</param>
        /// <returns>Modelo de la categoria creada</returns>
        public CategoryQuestionModel CreateCategoryQuestion(CategoryQuestionModel category){
            category.id_public = System.Guid.NewGuid().ToString();
            category.status = Enumerations.EnumGeneralStatus.Enabled;
            category.created_at = DateTime.Now;
            string id =_context.CategoryQuestions.Save(category);
            return _context.CategoryQuestions.Find(id);
        }

        /// <summary>
        /// Modifica una categoria de preguntas
        /// </summary>
        /// <param name="category">Modelo de una categoria a modificar</param>
        /// <returns>Modelo de la categoria modificadá</returns>
        public CategoryQuestionModel EditCategoryQuestion(CategoryQuestionModel category){
            List<CategoryQuestionModel> _categories = _context.CategoryQuestions.ToList();
            if(_categories.Any(x => x.id_public == category.id_public)){
                CategoryQuestionModel _category  = _categories.First(x => x.id_public == category.id_public);
                _category.name = category.name;
                _category.updated_at = DateTime.Now;
                _context.CategoryQuestions.Update(_category);
                return _category;
            }else{
                throw new ArgumentException("No se encontró la categoria.");
            }
        }

        /// <summary>
        /// Obtiene una categoria de preguntas
        /// </summary>
        /// <param name="id_public">Identificador publico de la categoria</param>
        /// <returns>Modelo de la categoria a obtener</returns>
        public CategoryQuestionModel GetCategoryQuestion(string id_public){
            List<CategoryQuestionModel> _categories = _context.CategoryQuestions.ToList();
            if(_categories.Any(x => x.id_public == id_public)){
                return _categories.First(x => x.id_public == id_public);
            }else{
                throw new ArgumentException("No se encontró la categoria.");
            }
        }

        /// <summary>
        /// Lista todas las categorias existentes, incluso las deshabilitadas
        /// </summary>
        /// <returns>Listado de categorias (CategoryQuestionModel)</returns>
        public List<CategoryQuestionModel> getAll(){
            return _context.CategoryQuestions.ToList();
        }

        /// <summary>
        /// Cambia de estado de una categoria
        /// </summary>
        /// <param name="id">id_public de la categoria</param>
        /// <returns>La categoria afectada(CategoryQuestionModel)</returns>
        public CategoryQuestionModel changeStatus(string id_public)
        {
            List<CategoryQuestionModel> _categories = _context.CategoryQuestions.ToList();
            if(_categories.Any(x => x.id_public == id_public)){
                CategoryQuestionModel _category = _categories.First(x => x.id_public == id_public);
                _category.status = _category.status == Enumerations.EnumGeneralStatus.Disabled ? Enumerations.EnumGeneralStatus.Enabled : Enumerations.EnumGeneralStatus.Disabled; 
                _category.updated_at = DateTime.Now;
                _context.CategoryQuestions.Update(_category);
                return _category;
            }else{
                throw new ArgumentException("No se encontró la categoria.");
            }
        }

    }
}