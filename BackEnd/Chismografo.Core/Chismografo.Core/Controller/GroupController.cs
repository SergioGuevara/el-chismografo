using Chismografo.Core.Interfaces;
using Chismografo.Core.DAL.Controller;
using Chismografo.Core.DAL.Interfaces;
using Chismografo.Core.Models;
using Chismografo.Core.Models.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using NotificationServices.Client;
using NotificationsModels =  NotificationServices.Models;
using EnumerationsNotification =  NotificationServices.Models.Util;

namespace Chismografo.Core.Controller
{
    public class GroupController : IGroup
    {
        public IContext _context { get; set; }

        public GroupController()
        {
            if(_context == null){
                DataFactory factory = new DataFactory();
                _context = factory.GetDataInstance();
            }
        }
        
        #region Group Methods

        /// <summary>
        /// Crear un grupo para un usuario determinado y si tiene listado de invitados envia la invitación
        /// </summary>
        /// <param name="group">Modelo de grupo ha crear</param>
        /// <returns>Modelo de grupo creado</returns>
        public GroupModel CreateGroup(GroupModel group){
            if(group.name != null){
                List<UserModel> _users = _context.Users.ToList();
                if(_users
                    .Any( x=> x.id_public == group.creator_user.id_public
                        && x.status == (int)Enumerations.EnumGeneralStatus.Enabled
                    )
                ){
                    group.id_public = Guid.NewGuid().ToString();
                    group.status = Enumerations.EnumGeneralStatus.Enabled;
                    group.creator_user = _users.First(x => x.id_public == group.creator_user.id_public);
                    group.created_at = DateTime.Now;
                    group.updated_at = DateTime.Now;
                    if(group.members == null)
                        group.members = new List<UserModel>();
                    group.members.Add(_users.First(x => x.id_public == group.creator_user.id_public));
                    string id = _context.Groups.Save(group);

                    group.members?.All(x=> {
                        if(_users.Any(y=> y.id_public == x.id_public)){
                            string id_user = _users.First(y=> y.id_public == x.id_public).Id.ToString();
                            this.SendInvitation(id, id_user, Enumerations.EnumInvitationType.App);
                            NotificationClient.CreateNotification(new NotificationsModels.NotificationModel(){
                                notification_type = EnumerationsNotification.Enumerations.EnumNotificationType.Email,
                                from = null,
                                subject = "Invitacón a grupo " + group.name,
                                body = "",
                                access_link = "",
                                user = new NotificationsModels.UserModel(){
                                    Id = x.Id,
                                    id_public = x.id_public,
                                    name = x.name,
                                    lastname = x.lastname,
                                    document_type =  x.document_type == null ? null : new NotificationsModels.DocumentTypeModel(){
                                        Id = x.document_type.Id,
                                        name = x.document_type.name,
                                        status = NotificationsModels.Util.Enumerations.EnumGeneralStatus.Enabled,
                                        created_at = DateTime.Now,
                                        updated_at = DateTime.Now
                                    },
                                    celphone = x.celphone,
                                    email = x.email,
                                    status = x.status,
                                    created_at = DateTime.Now,
                                    updated_at = DateTime.Now

                                }
                            });
                        }
                        return true;
                    });

                    return _context.Groups.Find(id);
                }else{
                    throw new ArgumentException("El usuario no se encontró.");
                }
            }else{
                throw new ArgumentException("El grupo debe tener un nombre");
            }    
        }

        /// <summary>
        /// Edita la info de un grupo 
        /// </summary>
        /// <param name="id_user">Identificación publica del usuario logeado</param>
        /// <param name="group">Modelo de grupo ha modificar</param>
        /// <returns>Modelo de grupo creado</returns>
        public GroupModel EditGroup(string id_user, GroupModel group){
            List<GroupModel> _groups = _context.Groups.ToList();
            if(_groups.Any( x=> x.id_public == group.id_public )){
                GroupModel _group = _groups.First( x=> x.id_public == group.id_public );

                _group.name = group.name;
                _group.updated_at = DateTime.Now;
                _context.Groups.Update(_group);

                return _context.Groups.Find(_group.Id.ToString());
            }else{
                throw new ArgumentException("El grupo no fue encontradó.");
            }
        }

        /// <summary>
        /// Elimina un grupo
        /// </summary>
        /// <param name="id_user">Identificación publica del usuario logeado</param>
        /// <param name="id_public">Identificación publica del grupo</param>
        public void DeleteGroup(string id_user, string id_public){
            List<GroupModel> groups = _context.Groups.ToList();
            if(groups.Any(x => x.id_public == id_public)){
                GroupModel _group = groups.First(x=> x.id_public == id_public);
                if(_group.creator_user.id_public == id_user){
                    _group.status = Enumerations.EnumGeneralStatus.Disabled;
                    _group.updated_at = DateTime.Now;
                    _context.Groups.Update(_group);
                }else{
                    throw new ArgumentException("No estas autirizado de eliminar este grupo");
                }
            }else{
                throw new ArgumentException("El grupo no fue encontradó.");
            }
        }

        /// <summary>
        /// Salida de un miembre de un grupo
        /// </summary>
        /// <param name="id_user">id publico del usuario que saldra del grupo</param>
        /// <param name="id_group">id publico del grupo</param>
        /// <returns>Valor Bool para identificar si la tarea se hizo correctamente</returns>
        public GroupModel LeaveGroup(string id_user, string id_group){
            List<GroupModel> groups = _context.Groups.ToList();
            if(groups.Any(x => x.id_public == id_group)){
                GroupModel group = groups.First( x => x.id_public == id_group);
                if(group.members.Any( x => x.id_public == id_user)){
                    group.updated_at = DateTime.Now;
                    UserModel member = group.members.First(x => x.id_public == id_user);
                    member.status = (int)Enumerations.EnumMemberStatus.Outside;
                    _context.Groups.Update(group);
                return _context.Groups.Find(group.Id.ToString());
                }else{
                    throw new ArgumentException("El usuario no pertenece al grupo.");
                }
            }else{
                throw new ArgumentException("No se encontró el grupo");
            }
        }

        /// <summary>
        /// Obtine un grupo
        /// </summary>
        /// <param name="id_public">id publico del grupo</param>
        /// <returns>Modelo del grupo encontrado</returns>
        public GroupModel GetGroup(string id_public){
            List<GroupModel> groups = _context.Groups.ToList();
            if(groups.Any(x=> x.id_public == id_public)){
                GroupModel _group = groups.First(x=> x.id_public == id_public);
                if( _group.questions != null)
                    _group.questions = _group.questions.Where(x=> x.status == Enumerations.EnumGeneralStatus.Enabled).ToList();
                return _group;
            }else{
                throw new ArgumentException("No se encontro el grupo.");
            }
        }

        /// <summary>
        /// Obtiene los grupos de un usuario
        /// </summary>
        /// <param name="id_public">id publico del usuario</param>
        /// <returns>Listado de modelos del grupos de un usuario</returns>
        public List<GroupModel> GetGroupsOfUser(string id_public){
            return _context.Groups.ToList().Where(x=> x.status == Enumerations.EnumGeneralStatus.Enabled 
            && x.members.Any(y=> y.id_public == id_public)).ToList();
        }

        /// <summary>
        /// Envia la invitación a usuarios para unirsen a un grupo (App/Email/Facebook)
        /// </summary>
        /// <param name="group">Modelo de grupo al cual se realizará la invitación</param>
        /// <returns>Valor Bool para identificar si la tarea se hizo correctamente</returns>
        public bool SendInvitation(string id_group, string id_user, Enumerations.EnumInvitationType invitation_type){
            InvitationModel invitation = new InvitationModel(){
                invitation_type = invitation_type,
                group = _context.Groups.Find(id_group),
                guest = _context.Users.Find(id_user),
                status = Enumerations.EnumStatusInvitaction.unRead,
                created_at = DateTime.Now,
                updated_at = DateTime.Now
            };
            string id = _context.Invitations.Save(invitation);
            return true;
        }

        /// <summary>
        /// Genera link de invitación a un grupo
        /// </summary>
        /// <param name="id_group">Id publico del grupo</param>
        /// <returns>Link de invitación</returns>
        public string GetInvitationLink(string id_group){
            List<GroupModel> _groups = _context.Groups.ToList();
            if(_groups.Any( x=> x.id_public == id_group)){
                GroupModel _group = _groups.First( x=> x.id_public == id_group);

                InvitationModel invitation = new InvitationModel(){
                    invitation_type = Enumerations.EnumInvitationType.Link,
                    invitation_link = Guid.NewGuid().ToString(),
                    group = _group,
                    guest = null,
                    status = Enumerations.EnumStatusInvitaction.unRead,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now
                };
                string id = _context.Invitations.Save(invitation);
                return invitation.invitation_link;
            }else{
                throw new ArgumentException("No se encontró el grupo.");
            }            
        }

        #endregion 
        
        #region Question 

        /// <summary>
        /// Crea una pregunta dentro de un grupo
        /// </summary>
        /// <param name="id_group">Identificación del grupo</param>
        /// <param name="question">Modelo de la pregunta que se va ha agregar al grupo</param>
        /// <returns>Modelo del grupo afectado</returns>
        public GroupModel CreateQuestion(string id_group, QuestionModel question){
            List<GroupModel> groups = _context.Groups.ToList();
            if(groups.Any(x => x.id_public == id_group)){
                GroupModel group = groups.First(x => x.id_public == id_group);
                List<CategoryQuestionModel> categories = _context.CategoryQuestions.ToList();
                if(categories.Any( x=> x.id_public == question.category.id_public)){
                    //question.Id = new MongoDB.Bson.ObjectId();
                    question.category = categories.First(x=> x.id_public == question.category.id_public);
                    question.id_public = Guid.NewGuid().ToString();
                    question.status = Enumerations.EnumGeneralStatus.Enabled;
                    question.created_at = DateTime.Now;
                    question.updated_at = DateTime.Now;
                    if(group.questions == null)
                        group.questions = new List<QuestionModel>();
                    group.questions.Add(question);
                    _context.Groups.Update(group);
                    return _context.Groups.ToList().First(x=> x.Id == group.Id);
                }else{
                    throw new ArgumentException("La categoria no se encontró.");
                }
            }else{
                throw new ArgumentException("No el grupo no existe"); 
            }
        }

        /// <summary>
        /// Modifica una pregunta dentro de un grupo
        /// </summary>
        /// <param name="question">Modelo de la pregunta que se va ha modificar</param>
        /// <returns>Modelo del grupo afectado</returns>
        public GroupModel EditQuestion(QuestionModel question){
            List<GroupModel> _groups = _context.Groups.ToList();
            if(_groups
                .Any(x=> x.questions
                    .Any( y=> y.id_public == question.id_public)
                )
            ){
                GroupModel _group = _groups.First(x=> x.questions.Any( y=> y.id_public == question.id_public));
                
                QuestionModel _question = _group.questions.First(x => x.id_public == question.id_public);
                _question.question = question.question;
                _question.updated_at = DateTime.Now;
                _context.Groups.Update(_group);  
                return _context.Groups.Find(_group.Id.ToString());
            }else{
                throw new ArgumentException("La pregunta no fue encontró.");
            }
        }

        /// <summary>
        /// Elimina una pregunta dentro de un grupo
        /// </summary>
        /// <param name="id_user">Identificación publica del usuario logeado</param>
        /// <param name="id_question">Identificación publica de una pregunta que se va ha elimnar del grupo</param>
        /// <returns>Modelo del grupo afectado</returns>
        public void DeleteQuestion(string id_user, string id_question){
            List<GroupModel> _groups = _context.Groups.ToList();
            if(_groups
                .Any(x=> x.questions
                    .Any( y=> y.id_public == id_question)
                )
            ){
                GroupModel _group = _groups.First(x=> x.questions.Any( y=> y.id_public == id_question));

                QuestionModel _question = _group.questions.First(x => x.id_public == id_question);
                _question.status = Enumerations.EnumGeneralStatus.Disabled;
                _question.updated_at = DateTime.Now;
                //group.updated_at = DateTime.Now;
                _context.Groups.Update(_group);
            }else{
                throw new ArgumentException("La pregunta no fue encontradá.");
            }
        }

        /// <summary>
        /// Obtine un modelo de una pregunta, dentro de un grupo
        /// </summary>
        /// <param name="id_public">Identificador publico de la pregunta</param>
        /// <returns>Modelo de la pregunta encontrada</returns>
        public QuestionModel GetQuestion(string id_public){
            List<GroupModel> _groups = _context.Groups.ToList();
            if(_groups
                .Any( x=> x.questions
                    .Any( z => z.id_public == id_public)
                )
            ){
                GroupModel _group = _groups
                .First( x=> x.questions
                    .Any( z => z.id_public == id_public)
                );

                return _group.questions.First(x => x.id_public == id_public);
            }else{
                throw new ArgumentException("No se encontro la pregunta.");
            }
        }

        #endregion

        #region Answer

        /// <summary>
        /// Responde una pregunta de un grupo
        /// </summary>
        /// <param name="id_question">Identificación publica de la pregunta</param>
        /// <param name="id_user">Identificación publica del usuario que esta dando la respuesta</param>
        /// <param name="answer">Modelo de la respuesta</param>
        /// <returns>Modelo del grupo afectado</returns>
        public GroupModel AnswerQuestion(string id_question, string id_user, AnswerModel answer){
            List<GroupModel> _groups = _context.Groups.ToList();
            if(_groups
                .Any(x=> x.questions
                    .Any( y=> y.id_public == id_question)
                )
            ){
                GroupModel _group = _groups.First(x=> x.questions.Any( y => y.id_public == id_question));
                
                QuestionModel _question = _group.questions.First(x => x.id_public == id_question);
                answer.status = Enumerations.EnumGeneralStatus.Enabled;
                answer.created_at = DateTime.Now;
                answer.updated_at = DateTime.Now;
                answer.user = _context.Users.ToList().First(x => x.id_public == id_user);
                _question.answers.Add(answer);
                //question.updated_at = DateTime.Now;
                //group.updated_at = DateTime.Now;
                _context.Groups.Update(_group);
                return _context.Groups.ToList().First(x=> x.Id == _group.Id);  
               
            }else{
                throw new ArgumentException("La pregunta no fue encontradá.");
            }
        }

        /// <summary>
        /// Eliminar una resputa
        /// </summary>
        /// <param name="id_answer">Identificación de la respuesta</param>
        /// <param name="id_user">Identificación publica del usuario se encuentra logeoado</param>
        /// <returns>Modelo del grupo afectado</returns>
        public GroupModel DeleteAnswer(string id_answer, string id_user){
            Tuple<GroupModel, AnswerModel> oResponse = GetAnswer(id_answer);

            if(oResponse.Item2.user.id_public != id_user){
                throw new ArgumentException("El usuario actual no puede realizar dicha tarea.");
            }
            
            oResponse.Item2.status = Enumerations.EnumGeneralStatus.Disabled;
            oResponse.Item2.updated_at = DateTime.Now;

            _context.Groups.Update(oResponse.Item1);

            return _context.Groups.Find(oResponse.Item1.Id.ToString());
        }

        /// <summary>
        /// Editar una resputa
        /// </summary>
        /// <param name="id_user">Identificación publica del usuario se encuentra logeoado</param>
        /// <param name="answer">Modelo de la respuesta</param>
        /// <returns>Modelo del grupo afectado</returns>
        public GroupModel EditAnswer(string id_user, AnswerModel answer){
            Tuple<GroupModel, AnswerModel> oResponse = GetAnswer(answer.Id.ToString());

            if(oResponse.Item2.user.id_public != id_user){
                throw new ArgumentException("El usuario actual no puede realizar dicha tarea.");
            }
            
            answer.text = answer.text;
            answer.updated_at = DateTime.Now;

            _context.Groups.Update(oResponse.Item1);

            return _context.Groups.Find(oResponse.Item1.Id.ToString());
        }
    
        /// <summary>
        /// Consulta el listado de respuestas para una pregunta
        /// </summary>
        /// <param name="id_question">Identificación de la pregunta</param>
        /// <returns>Listado de resputas </returns>
        public List<AnswerModel> GetAnswers(string id_question){
            List<GroupModel> _groups = _context.Groups.ToList();
            if(_groups
                .Any( x=> x.questions
                    .Any( y=> y.id_public == id_question)
                )
            ){
                GroupModel _group = _groups
                .First( x=> x.questions
                    .Any( y=> y.id_public == id_question)
                );

                QuestionModel _question = _group.questions.First(x => x.id_public == id_question);

                List<AnswerModel> oResponse = new List<AnswerModel>();
                if(_question.answers != null){
                    oResponse = _question.answers.Where(x => x.status == Enumerations.EnumGeneralStatus.Enabled).ToList();
                }

                _group.members?.Where(x => x.status == (int)Enumerations.EnumGeneralStatus.Enabled).ToList().All(x=>{
                    if(!oResponse.Any(y=> y.user.Id == x.Id)){
                        oResponse.Add(new AnswerModel(){
                            text = "Sin Responder",
                            user = x
                        });
                    }
                    return true;
                });

                return oResponse;
            }else{
                throw new ArgumentException("No se encontro la pregunta.");
            }
        }


        /// <summary>
        /// Editar una resputa
        /// </summary>
        /// <param name="id_answer">Identificación de la respuesta que quiere consultar</param>
        /// <returns>Tupla con el modelo del grupo de la respuesta (Item1) y el modelo de la respuesta (Item2) </returns>
        private Tuple<GroupModel,AnswerModel> GetAnswer(string id_answer){
            List<GroupModel> _groups = _context.Groups.ToList();
            if(_groups
                .Any( x=> x.questions
                    .Any( y=> y.answers
                        .Any( z => z.Id.ToString() == id_answer)
                    )
                )
            ){
                GroupModel _group = _groups
                .First( x=> x.questions
                    .Any( y=> y.answers
                        .Any( z => z.Id.ToString() == id_answer)
                    )
                );

                QuestionModel _question = _group.questions.First(x => x.answers.Any(y => y.Id.ToString() == id_answer));

                AnswerModel _answer = _question.answers.First(x => x.Id.ToString() == id_answer);

                Tuple<GroupModel, AnswerModel> oResponse = new Tuple<GroupModel, AnswerModel>(_group, _answer);
                return oResponse;
            }else{
                throw new ArgumentException("No se encontro la respuesta.");
            }
        }

        #endregion

    }
}