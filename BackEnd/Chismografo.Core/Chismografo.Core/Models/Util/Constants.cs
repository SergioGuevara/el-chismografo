namespace Chismografo.Core.Models.Util
{
    public class Constants
    {
        public const string C_LS_ProviderConnectionName = "LS_ProviderConnection";
        public const string C_LS_TypeProviderName = "LS_TypeProvider";
    }
}