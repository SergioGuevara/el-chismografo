namespace Chismografo.Core.Models.Util
{
    public class Enumerations
    {

        #region General

        public enum EnumGeneralStatus
        {
            Enabled = 1,
            Disabled = 0
        }
            
        #endregion

        #region Group

        public enum EnumMemberStatus
        {
            Inside = 002,
            Outside = 001,
        }

        #region Invitation

        public enum EnumInvitationType
        {
            App = 001,
            Link = 002
        }

        public enum EnumStatusInvitaction
        {
            unRead = 1,
            Read = 2,
            Rejected = 0
        }
            
        #endregion
            
        #endregion

    }
}