using MongoDB.Bson;
using System.Collections.Generic;
using System;

namespace Chismografo.Core.Models
{
    public class NotificationModel
    {
        public ObjectId Id { get; set; }
        public int NotificationType { get; set; }
        public string From { get; set; }
        public List<string> SendTo { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int statusNotification { get; set; }
        public string User { get; set; }
        public DateTime SentDate { get; set; }
        public DateTime Date { get; set; }
    }
}