using MongoDB.Bson;
using System;
using System.Collections.Generic;
using Chismografo.Core.DAL.DAOs.MongoDB.Interfaces;

namespace Chismografo.Core.Models
{
    public class UserModel : IModel
    {
        public ObjectId Id { get; set; }
        public string id_public { get; set; }
        public DocumentTypeModel document_type { get; set; }
        public string identification { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string celphone { get; set; }
        public string photo { get; set; }
        public string email { get; set; }
        public SocialModel social_info { get; set; }
        public int status { get; set; }  
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        
    }
}