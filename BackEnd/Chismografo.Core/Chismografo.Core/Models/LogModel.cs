using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Chismografo.Core.DAL.DAOs.MongoDB.Interfaces;

namespace Chismografo.Core.Models
{
    public class LogModel : IModel
    {
        public ObjectId Id { get; set; }
        public string Process { get; set; }

        public List<TaskModel> Tasks { get; set; }
        public string User { get; set; }
        public DateTime Date { get; set; }
    }
}