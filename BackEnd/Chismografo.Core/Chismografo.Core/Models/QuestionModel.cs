using MongoDB.Bson;
using System.Collections.Generic;
using System;
using Chismografo.Core.Models.Util;
using Chismografo.Core.DAL.DAOs.MongoDB.Interfaces;

namespace Chismografo.Core.Models
{
    public class QuestionModel : IModel
    {
        public ObjectId Id { get; set; }
        public string id_public  { get; set; }
        public string question { get; set; }
        public CategoryQuestionModel category { get; set; }
        public List<AnswerModel> answers { get; set; }
        public Enumerations.EnumGeneralStatus status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}