using MongoDB.Bson;
using Chismografo.Core.Models.Util;
using System.Collections.Generic;
using System;
using Chismografo.Core.DAL.DAOs.MongoDB.Interfaces;

namespace Chismografo.Core.Models
{
    public class InvitationModel : IModel
    {
        public ObjectId Id { get; set; }
        public Enumerations.EnumInvitationType invitation_type { get; set; }
        public string invitation_link { get; set; }
        public Enumerations.EnumGeneralStatus link_status { get; set; }
        public GroupModel group { get; set; }
        public UserModel guest { get; set; }
        public Enumerations.EnumStatusInvitaction status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}