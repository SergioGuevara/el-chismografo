using System.Collections.Generic;
using MongoDB.Bson;
using System;
using Chismografo.Core.Models.Util;
using Chismografo.Core.DAL.DAOs.MongoDB.Interfaces;

namespace Chismografo.Core.Models
{
    public class GroupModel : IModel
    {
        public ObjectId Id { get; set; }
        public string id_public { get; set; }
        public string name { get; set; }
        public string image { get; set; }
        public List<UserModel> members { get; set; }
        public List<QuestionModel> questions { get; set; }
        public UserModel creator_user { get; set; }
        public Enumerations.EnumGeneralStatus status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}