namespace Chismografo.Core.Models
{
    public class SocialModel
    {
        public string id { get; set; }  
        public string email { get; set; } 
        public string provider { get; set; } 
    }
}   