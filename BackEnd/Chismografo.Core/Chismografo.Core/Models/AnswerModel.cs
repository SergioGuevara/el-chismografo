using MongoDB.Bson;
using System;
using Chismografo.Core.Models.Util;

namespace Chismografo.Core.Models
{
    public class AnswerModel
    {
        public ObjectId Id { get; set; }
        public string text { get; set; }
        public UserModel user { get; set; }
        public Enumerations.EnumGeneralStatus status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        
    }
}