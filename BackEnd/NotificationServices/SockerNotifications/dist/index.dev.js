"use strict";

var app = require('express')();

var server = require('http').createServer(app);

var io = require('socket.io')(server, {
  cors: {
    origin: "http://localhost:4200",
    methods: ["GET", "POST"]
  }
});

var axios = require('axios');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
var config = {
  method: 'get',
  url: 'https://localhost:5001/user/get-notifications',
  headers: {
    'Content-Type': 'application/json'
  },
  data: JSON.stringify({
    "id_public": "4f2c8562-30bb-4df0-8d1e-dc221c1b752b",
    "document_type_id": null,
    "identification": null,
    "name": "sergio Alexander",
    "lastname": "Palacios Guevara",
    "celphone": null,
    "photo": "https://lh3.googleusercontent.com/a-/AOh14GgnJ-pa35fcwb_mj3U5johC5TEZ_nBjyYTGlQInWjQ=s96-c",
    "email": "sergiosk1000@gmail.com",
    "status": 1,
    "social_info": {
      "id": null,
      "name": null,
      "email": null,
      "photoUrl": null,
      "firstName": null,
      "lastName": null,
      "authToken": null,
      "idToken": null,
      "provider": null
    },
    "created_at": "2020-11-25T22:47:55.972Z",
    "updated_at": "2020-11-25T22:47:55.972Z"
  })
};
io.on('connection', function (socket) {
  socket.on('send-notification', function (u) {
    console.log(u);
    config.data = u;
    axios(config).then(function (response) {
      //console.log(JSON.stringify(response.data));
      socket.emit('notification-event', response.data);
    })["catch"](function (error) {
      console.log(error);
    });
  });
});
s;
server.listen(3000, function () {
  console.log('listening on *:3000');
});