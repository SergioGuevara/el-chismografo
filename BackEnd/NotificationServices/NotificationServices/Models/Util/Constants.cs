namespace NotificationServices.Models.Util
{
    public class Constants
    {
        public const string C_LS_ProviderConnectionName = "NS_ProviderConnection";
        public const string C_LS_TypeProviderName = "SS_TypeProvider";
    }
}