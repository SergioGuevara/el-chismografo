namespace NotificationServices.Models.Util
{
    public class Enumerations{

        public enum EnumGeneralStatus
        {
            Enabled = 1,
            Disabled = 0
        }

        public enum EnumNotificationType
        {
            Web = 001,
            Email = 002,
            SMS = 003,
            FaceBook = 004
        }

        public enum EnumNotificationStatus
        {
            Created = 001,
            Sent = 002,
            Read = 003
        }

    }
}