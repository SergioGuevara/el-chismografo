using System;
using System.Collections.Generic;
using MongoDB.Bson;
using NotificationServices.DAL.DAOs.MongoDB.Interfaces;
using NotificationServices.Models.Util;

namespace NotificationServices.Models
{
    public class NotificationModel : IModel
    {
        public ObjectId Id { get; set; }
        public Enumerations.EnumNotificationType notification_type { get; set; }
        public string from { get; set; }
        public List<string> send_to_emails { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string access_link { get; set; }
        public Enumerations.EnumNotificationStatus status_notification { get; set; }
        public UserModel user { get; set; }
        public DateTime sent_date { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}