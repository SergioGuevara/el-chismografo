using MongoDB.Bson;
using NotificationServices.Models.Util;
using System;
using NotificationServices.DAL.DAOs.MongoDB.Interfaces;

namespace NotificationServices.Models
{
    public class DocumentTypeModel : IModel
    {
        public ObjectId Id { get; set; }
        public string name { get; set; }
        public Enumerations.EnumGeneralStatus status { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}