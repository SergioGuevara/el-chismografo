using NotificationServices.Models;
using System.Collections.Generic;

namespace NotificationServices.Interfaces
{
    public interface INotification
    {
        /// <summary>
        /// Crear un notificación
        /// </summary>
        /// <param name="notificacion">Modelo de la notificación</param>
        /// <returns>Modelo de la notificación creada</returns>
        NotificationModel CreateNotification(NotificationModel notification);

        /// <summary>
        /// Envia un correo electronico
        /// </summary>
        /// <param name="element">Modelo de una notificación</param>
        /// <returns>boleano confirmando la tarea</returns>
        bool SendEmail(NotificationModel element);

        /// <summary>
        /// Retorna las notificacion sin leer (APP) para un usuario en especifico
        /// </summary>
        /// <param name="element">id publico de Usuario en session</param>
        /// <returns>Lista de notificaciones</returns>
        List<NotificationModel> GetNotificationByUser(string id_public);
    }
}