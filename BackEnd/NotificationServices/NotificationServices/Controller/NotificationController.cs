using NotificationServices.Interfaces;
using NotificationServices.DAL.Controller;
using NotificationServices.DAL.Interfaces;
using NotificationServices.Models.Util;
using NotificationServices.Models;
using System.Net.Mail;
using System.Net;
using System;
using System.Linq;
using System.Collections.Generic;

namespace NotificationServices.Controller
{
    internal class LogController : INotification
    {
        public IContext _context { get; set; }

        public LogController()
        {
            if(_context == null){
                DataFactory factory = new DataFactory();
                _context = factory.GetDataInstance();
            }
        }

        /// <summary>
        /// Crear un notificación
        /// </summary>
        /// <param name="notificacion">Modelo de la notificación</param>
        /// <returns>Modelo de la notificación creada</returns>
        public NotificationModel CreateNotification(NotificationModel notification){
            notification.status_notification = Enumerations.EnumNotificationStatus.Created;
            notification.created_at = DateTime.Now;
            notification.updated_at = DateTime.Now;
            string id = _context.Notifications.Save(notification);
            return _context.Notifications.ToList().First(x => x.Id.ToString() == id);
        }

        /// <summary>
        /// Envia un correo electronico
        /// </summary>
        /// <param name="element">Modelo de una notificación</param>
        /// <returns>boleano confirmando la tarea</returns>
        public bool SendEmail(NotificationModel element){
            try{
                element.created_at = DateTime.Now;
                string idNotification = _context.Notifications.Save(element);

                var mailMessage = new MailMessage
                {
                    From = new MailAddress(element.from),
                    Subject = element.subject,
                    Body = element.body,
                    IsBodyHtml = true,
                };
                foreach(string a in  element.send_to_emails){
                    mailMessage.To.Add(a);
                }

                var smtpClient = new SmtpClient("smtp.gmail.com")
                {
                    Port = 587,
                    Credentials = new NetworkCredential("sergiosk1000@gmail.com", "sergiopalacios27+"),
                    EnableSsl = true,
                };
                    
                smtpClient.Send(mailMessage);

                var toUpdate = _context.Notifications.Find(idNotification);
                toUpdate.status_notification = Enumerations.EnumNotificationStatus.Sent;
                toUpdate.sent_date = DateTime.Now;
                _context.Notifications.Update(toUpdate);
                return true;
            }catch(Exception e){
                return false;
            }
        }
    
        /// <summary>
        /// Retorna las notificacion sin leer (APP) para un usuario en especifico
        /// </summary>
        /// <param name="element">id publico de Usuario en session</param>
        /// <returns>Lista de notificaciones</returns>
        public List<NotificationModel> GetNotificationByUser(string id_public){
            return _context.Notifications.ToList()
            .Where(x=> x.status_notification == Enumerations.EnumNotificationStatus.Created
            && x.user.id_public == id_public)
            .ToList();
        }
    }
}