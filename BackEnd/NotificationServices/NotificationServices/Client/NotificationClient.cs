using System.Collections.Generic;
using NotificationServices.Models;
using NotificationServices.Interfaces;
using NotificationServices.Controller;
using Autofac;

namespace NotificationServices.Client
{
    public static class NotificationClient
    {
        /// <summary>
        /// Crear un notificación
        /// </summary>
        /// <param name="notificacion">Modelo de la notificación</param>
        /// <returns>Modelo de la notificación creada</returns>
        public static NotificationModel CreateNotification(NotificationModel notification){
            var builder = new ContainerBuilder();
            builder.RegisterType<LogController>().As<INotification>();

            var container = builder.Build();
            return container.Resolve<INotification>().CreateNotification(notification);
        }

        /// <summary>
        /// Envia un correo electronico
        /// </summary>
        /// <param name="element">Modelo de una notificación</param>
        /// <returns>boleano confirmando la tarea</returns>
        public static bool SendEmail(NotificationModel element){
            var builder = new ContainerBuilder();
            builder.RegisterType<LogController>().As<INotification>();

            var container = builder.Build();
            return container.Resolve<INotification>().SendEmail(element);
        }

        /// <summary>
        /// Retorna las notificacion sin leer (APP) para un usuario en especifico
        /// </summary>
        /// <param name="element">id publico de Usuario en session</param>
        /// <returns>Lista de notificaciones</returns>
        public static List<NotificationModel> GetNotificationByUser(string element){
            var builder = new ContainerBuilder();
            builder.RegisterType<LogController>().As<INotification>();

            var container = builder.Build();
            return container.Resolve<INotification>().GetNotificationByUser(element);
        }

    }
}