using NotificationServices.DAL.Interfaces;
using MongoDB.Driver;
using NotificationServices.Models.Util;

namespace NotificationServices.DAL.DAOs.MongoDB
{
    internal class MongoDBConnection : IConnection<IMongoDatabase>
    {
        public IMongoDatabase _database { get; set; }

        public MongoDBConnection()
        {
            var client = new MongoClient();
            //_database = client.GetDatabase(Constants.C_LS_ProviderConnectionName); //"chismografo_log"
            _database = client.GetDatabase("chismografo"); 
        }

    }
}