using MongoDB.Bson;

namespace NotificationServices.DAL.DAOs.MongoDB.Interfaces
{
    internal interface IModel
    {
        ObjectId Id { get; set; }
    }
}