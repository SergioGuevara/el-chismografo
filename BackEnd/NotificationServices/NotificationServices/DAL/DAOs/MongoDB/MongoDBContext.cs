using NotificationServices.DAL.Interfaces;
using NotificationServices.Models;
using MongoDB.Driver;
using MongoDB.Bson;

namespace NotificationServices.DAL.DAOs.MongoDB
{
    internal class MongoDBContext : IContext
    {
        public MongoDBContext()
        {
        }

        public IConnection<MongoDBConnection> _connection { get; set; }

        public IADO<NotificationModel> Notifications { get{
            return new MongoDB_ADO<NotificationModel>("notification");
        } set {} }
    }
}