using NotificationServices.Models;
using NotificationServices.DAL.Interfaces;

namespace NotificationServices.DAL.Interfaces
{
    public interface IContext
    {
        IADO<NotificationModel> Notifications { get; set; }

    }
}