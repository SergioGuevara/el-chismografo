using System.Collections.Generic;

namespace NotificationServices.DAL.Interfaces
{
    public interface IADO<T>
    {

        List<T> ToList();

        T Find(string Id);
    
        void Update(T entity);

        string Save(T entity);
    }
}