namespace NotificationServices.DAL.Interfaces
{
    public interface IConnection<T>
    {
        T _database { get; set; }
    }
}