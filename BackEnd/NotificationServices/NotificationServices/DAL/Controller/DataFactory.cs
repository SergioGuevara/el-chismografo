using System;
using NotificationServices.Models.Util;

namespace NotificationServices.DAL.Controller
{
    public class DataFactory
    {
        public NotificationServices.DAL.Interfaces.IContext GetDataInstance(){
            //Type typetoreturn = Type.GetType(Constants.C_LS_TypeProviderName);//"NotificationServices.DAL.DAOs.MongoDB.MongoDBContext,Context"
            Type typetoreturn = Type.GetType("NotificationServices.DAL.DAOs.MongoDB.MongoDBContext,NotificationServices");
            NotificationServices.DAL.Interfaces.IContext oReturn = (NotificationServices.DAL.Interfaces.IContext)Activator.CreateInstance(typetoreturn);
            return oReturn;
        }
    }
}