using System;
using Xunit;
using NotificationServices.Client;
using NotificationServices.Models;
using NotificationServices.Models.Util;
using System.Collections.Generic;

namespace NotificationServices_Tests
{
    public class UnitTest1
    {
        [Fact]
        public void SendEmail()
        {
            NotificationModel t = new NotificationModel(){
                notification_type = Enumerations.EnumNotificationType.Email,
                from = "che@gmail.com",
                send_to_emails = new List<string>(){ "sergio.palacios.guevara@gmail.com", "sergiosk1000@gmail.com", "checho_cd@gmail.com" },
                subject = "asunto" ,
                body = "de otro" ,
                user = null
            };
            var oReturn = NotificationClient.SendEmail(t);
        }
    }
}
