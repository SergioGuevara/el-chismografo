import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

//Models Imported
import { ResponseModel } from '../models/util/response.model';
import { CategoryModel } from '../models/category.model';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

@Injectable()
export class CategoryService {

  //ATTRIBUTES
  url:string;
  response: ResponseModel;

  //CONSTRUCTOR
  constructor( private http: HttpClient) {
    this.url = `${ environment.rootBaseApi }`;
  }

  //METHODS
  createCategory(model: CategoryModel): Observable<ResponseModel> {
    return this.http.post<ResponseModel>(`${this.url}category/create`, model, httpOptions).pipe(
    );
  }

  getAll(): Observable<ResponseModel[]> {
    return this.http.get<ResponseModel[]>(`${this.url}categories`, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

  changeStatus(id_public:string){
    return this.http.post<ResponseModel>(`${this.url}category/changestatus?id=${id_public}`, null, httpOptions).pipe(
    );
  }

  edit(model: CategoryModel): Observable<ResponseModel>{
    return this.http.post<ResponseModel>(`${this.url}category/edit`, model, httpOptions).pipe(
    );
  }

  getCategory(id_public:string) : Observable<ResponseModel>{
    return this.http.get<ResponseModel>(`${this.url}category/${id_public}`, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

}