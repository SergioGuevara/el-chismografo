import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { io } from 'socket.io-client';

 
@Injectable()
export class NotificationService {

  //ATTRIBUTES
  socket:any;
  readonly uri: string = "ws://localhost:3000";

  //CONSTRUCTOR
  constructor() {
    this.socket = io(this.uri);
  }

  //METHODS
  listen(eventName:string){
    return new Observable( (subscribe) =>{
        this.socket.on(eventName, (data) => {
            subscribe.next(data);
        })
    } );
  }

  emit(eventName: string, data: any){
    this.socket.emit(eventName, data);
  }

}