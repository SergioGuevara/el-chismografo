import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

//Models
import { ResponseModel } from '../models/util/response.model';
import { GroupModel } from '../models/group.model';
import { AnswerModel } from '../models/answer.model';
import { QuestionModel } from '../models/question.model';
import { UserModel } from '../models/user.model';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

@Injectable()
export class GroupService {

  //ATTRIBUTES
  url:string;
  response: ResponseModel;

  //CONSTRUCTOR
  constructor( 
    private http: HttpClient
  ) {
    this.url = `${ environment.rootBaseApi }group`;
  }

  //METHODS



  // *********************************  GROUP METHODS ***************************** //

  createGroup(model: GroupModel): Observable<ResponseModel> {
    return this.http.post<ResponseModel>(`${this.url}/create`, model, httpOptions).pipe(
      
    );
  }

  editGroup(model: GroupModel): Observable<ResponseModel>{
    return this.http.post<ResponseModel>(`${this.url}/edit`, model, httpOptions).pipe(
    );
  }

  deleteGroup(id_public:string){
    return this.http.post<ResponseModel>(`${this.url}/delete/${id_public}`, null, httpOptions).pipe(
    );
  }

  getGroups(): Observable<ResponseModel[]> {
    return this.http.get<ResponseModel[]>(`${this.url}/getAll`, httpOptions).pipe(
      //tap(data => console.log('Groups List ' + JSON.stringify(data)))
    );
  }

  getGroup(id_public:string) : Observable<ResponseModel>{
    return this.http.get<ResponseModel>(`${this.url}/${id_public}`, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

  leaveGroup(id_public:string) : Observable<ResponseModel>{
    return this.http.post<ResponseModel>(`${this.url}/leave/${id_public}`, null, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

  sendInvitation(id_public:string, model:UserModel[]) : Observable<ResponseModel>{
    return this.http.post<ResponseModel>(`${this.url}/sendinvitation/${id_public}`,model, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

  getInvitationLink(id_group:string) : Observable<ResponseModel>{
    return this.http.post<ResponseModel>(`${this.url}/getinvitationlink/${id_group}`, null, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

  // *********************************  QUESTION METHODS ***************************** //

  createQuestion(model:QuestionModel) : Observable<ResponseModel>{
    return this.http.post<ResponseModel>(`${this.url}/create-question`, model, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

  editQuestion(model:QuestionModel) : Observable<ResponseModel>{
    return this.http.post<ResponseModel>(`${this.url}/edit-question`, model, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

  deleteQuestion(id_public:string){
    return this.http.post(`${this.url}/delete-question/${id_public}`, null, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

  getQuestion(id_public:string) : Observable<ResponseModel>{
    return this.http.get<ResponseModel>(`${this.url}/question/${id_public}`, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

  // *********************************  ANSWER METHODS ***************************** //

  answerQuestion(model:AnswerModel) : Observable<ResponseModel>{
    return this.http.post<ResponseModel>(`${this.url}/answer`, model, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

  deleteAnswer(id_public:string) : Observable<ResponseModel>{
    return this.http.post<ResponseModel>(`${this.url}/delete-answer/${id_public}`, null, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

  editAnswer(model:AnswerModel) : Observable<ResponseModel>{
    return this.http.post<ResponseModel>(`${this.url}/edit-answer`, model, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

  getAnswers(idQuestion:string) : Observable<ResponseModel>{
    return this.http.get<ResponseModel>(`${this.url}/get-answers/${idQuestion}`, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }
}