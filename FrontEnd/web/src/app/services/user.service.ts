import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ResponseModel } from '../models/util/response.model';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

@Injectable()
export class UserService {

  //ATTRIBUTES
  url:string;
  response: ResponseModel;

  //CONSTRUCTOR
  constructor( private httpClient: HttpClient) {
    this.url = `${ environment.rootBaseApi }api/question/create`;
  }

  //METHODS
}