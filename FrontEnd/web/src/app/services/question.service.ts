import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
//Services
import { CategoryService } from '../services/category.service';

//Models
import { ResponseModel } from '../models/util/response.model';
import { QuestionModel } from '../models/question.model';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

@Injectable()
export class QuestionService {

  //ATTRIBUTES
  url:string;
  response: ResponseModel;
  question: QuestionModel = new QuestionModel();

  //CONSTRUCTOR
  constructor( 
    private http: HttpClient,
    private _categoryService: CategoryService
  ) {
    this.url = `${ environment.rootBaseApi }question`;
  }

  //METHODS

  getQuestionModel(id_public?:string): QuestionModel{
     this._categoryService.getAll()
    .subscribe(this.showCategories.bind(this));
    return this.question;
  }

  private showCategories(data: ResponseModel) {
    this.question.categories = data.dataResponse
  }

  createQuestion(model: QuestionModel): Observable<QuestionModel> {
    return this.http.post<QuestionModel>(`${this.url}/create`, model, httpOptions).pipe(
      
    );
  }

  getEnabledQuestions(): Observable<QuestionModel[]> {
    return this.http.get<QuestionModel[]>(`${this.url}/getEnabledQuestions`, httpOptions).pipe(
      //tap(data => console.log('Question List ' + JSON.stringify(data)))
    );
  }

  changeStatus(id_public:string){
    return this.http.post<QuestionModel>(`${this.url}/changestatus?id=${id_public}`, null, httpOptions).pipe(
    );
  }

  edit(model: QuestionModel): Observable<QuestionModel>{
    return this.http.post<QuestionModel>(`${this.url}/edit`, model, httpOptions).pipe(
    );
  }

  getQuestion(id_public:string) : Observable<QuestionModel>{
    return this.http.get<QuestionModel>(`${this.url}/${id_public}`, httpOptions).pipe(
      //tap(data => console.log('Categories ' + JSON.stringify(data)))
    );
  }

}