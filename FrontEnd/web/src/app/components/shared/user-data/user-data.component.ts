import { Component, OnInit, ChangeDetectorRef, Output, EventEmitter, Input } from '@angular/core';
import { CreateGroupComponent } from '../../group/create-group/create-group.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { combineLatest, Subscription } from 'rxjs';
import { Router } from '@angular/router';

//Models
import { UserModel } from '../../../auth/models/user.model';
import { NotificationModel } from '../../../models/notification.model';
import { ResponseModel } from '../../../models/util/response.model';
import { enumTypeMessageResponse } from '../../../models/util/Enumerations.enum';

//Components
import { ListGroupComponent } from '../../group/list-group/list-group.component';

//Services
import { AuthenticationService } from '../../../auth/services/authentication.service';
import { NotificationService } from '../../../services/notification.service';

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.css']
})
export class UserDataComponent implements OnInit {

  //Notification Attributes
  notifications: NotificationModel[] = [];

  redirection_url: string = 'login';
  user: UserModel = this._authenticationService.getUser();
  loggedIn: boolean = this._authenticationService.socialLogeo;
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  @Output() refresh:EventEmitter<any> = new EventEmitter();
  @Input() countNotes: number;

  constructor(
    private _authenticationService: AuthenticationService,
    private modalService: BsModalService, 
    private changeDetection: ChangeDetectorRef,
    private _router: Router,
    private _notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this._notificationService.emit('send-notification', this._authenticationService.getUser());

    this._notificationService.listen('notification-event')
    .subscribe((data: ResponseModel)=>{
      if(data.status == enumTypeMessageResponse.Success){
        this.notifications = data.dataResponse;
      }
    });
  }

  // testSocket(){
  //   this._notificationService.emit('send-notification', this._authenticationService.getUser());
  //   console.log(this._authenticationService.getUser());
  // }

  refreshList(){
    this.refresh.emit();
  }

  opemModalToCreateGroup() {
    const initialState = {
    };
    this.bsModalRef = this.modalService.show(CreateGroupComponent, {initialState});
    
    const _combine = combineLatest(
      this.bsModalRef.onHide,
      this.bsModalRef.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.bsModalRef.onHidden.subscribe((reason: string | any) => {
        this.refreshList();
      })
    );
 
    this.subscriptions.push(_combine);
  }

  unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  signout(){
    this._authenticationService.Signout();
    this.redirect();
  }

  private redirect(){
    this._router.navigateByUrl("login")
    .then((a)=> console.log('bien'))
    .catch((a)=>console.log(a))
    .finally( () =>  console.log('finalizo'));
  }

}
