import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.css']
})
export class ConfirmModalComponent implements OnInit {

  modalRef: BsModalRef;
  @Input() message: string;
  @ViewChild("template") template;
  @Output('confirm') confirm_ref = new EventEmitter();

  constructor(private modalService: BsModalService) {}

  ngOnInit(): void {
  }

  openModal(){
    this.modalRef = this.modalService.show(this.template, {class: 'modal-sm'});
  }
 
  confirm(): void {
    this.confirm_ref.emit();
    this.modalRef.hide();
  }
 
  decline(): void {
    this.modalRef.hide();
  }

}
