import { Component, OnInit } from '@angular/core';

//Services
import { QuestionService } from '../../../services/question.service';

//Models
import { QuestionModel } from '../../../models/question.model';
import { ResponseModel } from '../../../models/util/response.model';

@Component({
  selector: 'app-list-question',
  templateUrl: './list-question.component.html',
  styleUrls: ['./list-question.component.css']
})
export class ListQuestionComponent implements OnInit {

  listQuestion: QuestionModel[] = [];

  constructor(
    private _questionService: QuestionService
  ) { }

  ngOnInit(): void {
    this.refresh();
  }

  refresh(){
    this._questionService.getEnabledQuestions().subscribe(
      this.showCategories.bind(this)
    );
  }

  showCategories(data: ResponseModel){
    this.listQuestion = data.dataResponse;
  }

  changeStatus(id_public:string){
    this._questionService.changeStatus(id_public).subscribe(
      data => this.refresh()
    );
  }

  

}
