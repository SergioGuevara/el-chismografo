import { Component, OnInit } from '@angular/core';

//Services
import { QuestionService } from '../../../services/question.service';

//Models
import { QuestionModel } from '../../../models/question.model';

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.css']
})
export class CreateQuestionComponent implements OnInit {

  question:QuestionModel;

  constructor(
    private _questionService: QuestionService
  ) {
    this.question = this._questionService.getQuestionModel();
  }

  ngOnInit(): void {
  }
  
  consultar():void {
    console.log(this.question);
  }

  save(): void {
    this._questionService.createQuestion(this.question)
    .subscribe(data => {
    });
  }

}
