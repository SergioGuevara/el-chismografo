import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

//Services
import { QuestionService } from '../../../services/question.service';

//Models
import { QuestionModel } from '../../../models/question.model';
import { ResponseModel } from '../../../models/util/response.model';

@Component({
  selector: 'app-update-question',
  templateUrl: './update-question.component.html',
  styleUrls: ['./update-question.component.css']
})
export class UpdateQuestionComponent implements OnInit {

  question:QuestionModel;

  constructor(
    private _questionService:QuestionService,
    private rutaActiva: ActivatedRoute
  ) { }

  ngOnInit(): void {
    let questionTemp = this.question = this._questionService.getQuestionModel();
    if(this.rutaActiva.snapshot.params != null){
      if(this.rutaActiva.snapshot.params.id != null){
        this._questionService.getQuestion(this.rutaActiva.snapshot.params.id)
        .subscribe((data) => {
          this.question = data["dataResponse"];
          this.question.categories = questionTemp.categories;
        });
      }
    }
  }

  update(){
    this._questionService.edit(this.question).subscribe();
  }

}
