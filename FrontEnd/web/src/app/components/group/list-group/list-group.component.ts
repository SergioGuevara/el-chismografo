import { Component, OnInit } from '@angular/core';

//Services
import { GroupService } from '../../../services/group.service';

//Models
import { GroupModel } from '../../../models/group.model';
import { ResponseModel } from '../../../models/util/response.model';

@Component({
  selector: 'app-list-group',
  templateUrl: './list-group.component.html',
  styleUrls: ['./list-group.component.css']
})
export class ListGroupComponent implements OnInit {

  listGroups: GroupModel[] = [];

  constructor(
    private _groupService: GroupService
  ) { }

  ngOnInit(): void {
    this.refresh();
  }

  refresh(){
    this._groupService.getGroups().subscribe(
      this.showCategories.bind(this)
    );
  }

  showCategories(data: ResponseModel){
    this.listGroups = data.dataResponse;
  }

  delete(id_public:string){
    this._groupService.deleteGroup(id_public).subscribe(
      data => this.refresh()
    );
  }

}
