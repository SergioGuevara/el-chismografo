import { Component, OnInit} from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { enumTypeMessageResponse } from '../../../models/util/Enumerations.enum'; 

//Services
import { GroupService } from '../../../services/group.service';
import { AuthenticationService } from '../../../auth/services/authentication.service';

//Models
import { GroupModel } from '../../../models/group.model';
import { ResponseModel } from '../../../models/util/response.model';

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.css']
})
export class CreateGroupComponent implements OnInit {

  group:GroupModel = new GroupModel();
  _enumTypeMessageResponse:any = enumTypeMessageResponse;

  constructor(
    private _groupService: GroupService,
    public bsModalRef: BsModalRef,
    public _authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
  }

  save(): void {
    this.group.creator_user = this._authenticationService.getUser();
    this._groupService.createGroup(this.group)
    .subscribe((data:ResponseModel) => {
      if(data.status == this._enumTypeMessageResponse.Success){
        this.bsModalRef.hide();
      }
    });
  }

}
