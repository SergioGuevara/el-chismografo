import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

//Services
import { GroupService } from '../../../services/group.service';

//Models
import { GroupModel } from '../../../models/group.model';
import { ResponseModel } from '../../../models/util/response.model';

@Component({
  selector: 'app-update-group',
  templateUrl: './update-group.component.html',
  styleUrls: ['./update-group.component.css']
})
export class UpdateGroupComponent implements OnInit {

  group:GroupModel;

  constructor(
    private _groupService:GroupService,
    private rutaActiva: ActivatedRoute
  ) { }

  ngOnInit(): void {
    if(this.rutaActiva.snapshot.params != null){
      if(this.rutaActiva.snapshot.params.id != null){
        this._groupService.getGroup(this.rutaActiva.snapshot.params.id)
        .subscribe((data) => this.group = data["dataResponse"] );
      }
    }
  }

  update(){
    this._groupService.editGroup(this.group).subscribe();
  }

}
