import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

//Services
import { GroupService } from '../../../../services/group.service';
import { QuestionService } from '../../../../services/question.service';

//Models
import { QuestionModel } from '../../../../models/question.model';
import { ResponseModel } from '../../../../models/util/response.model';
import { ConstantPool } from '@angular/compiler';

@Component({
  selector: 'app-update-group-question',
  templateUrl: './update-group-question.component.html',
  styleUrls: ['./update-group-question.component.css']
})
export class UpdateGroupQuestionComponent implements OnInit {

  question:QuestionModel;

  constructor(
    private _groupService:GroupService,
    private _questionService:QuestionService,
    private rutaActiva: ActivatedRoute
  ) { 
    let questionTemp = this.question = this._questionService.getQuestionModel();
    if(this.rutaActiva.snapshot.params != null){
      if(this.rutaActiva.snapshot.params.question != null){
        this._groupService.getQuestion(this.rutaActiva.snapshot.params.question)
        .subscribe((data) => { 
          this.question = data["dataResponse"]; 
          this.question.categories = questionTemp.categories;
          this.question.id_public_group = this.rutaActiva.snapshot.params.id;
        });
      }
    }
  }

  ngOnInit(): void {    
  }

  update(){
    this._groupService.editQuestion(this.question).subscribe();
  }

}
