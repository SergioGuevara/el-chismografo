import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateGroupQuestionComponent } from './update-group-question.component';

describe('UpdateGroupQuestionComponent', () => {
  let component: UpdateGroupQuestionComponent;
  let fixture: ComponentFixture<UpdateGroupQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateGroupQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGroupQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
