import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

//Services
import { GroupService } from '../../../../services/group.service';

//Models
import { GroupModel } from '../../../../models/group.model';
import { ResponseModel } from '../../../../models/util/response.model';
import { AnswerModel } from 'src/app/models/answer.model';
import { QuestionModel } from 'src/app/models/question.model';

// Components
import { ConfirmModalComponent } from 'src/app/components/shared/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-list-group-question',
  templateUrl: './list-group-question.component.html',
  styleUrls: ['./list-group-question.component.css']
})
export class ListGroupQuestionComponent implements OnInit {

  message: string = null;
  @ViewChild(ConfirmModalComponent) confirm_modal:ConfirmModalComponent; 
  group: GroupModel = new GroupModel();
  countGroups: number = 0;
  listAnswers: AnswerModel[] = [];
  question: QuestionModel = new QuestionModel();

  constructor(
    private _groupService: GroupService,
    private rutaActiva: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    if(this.rutaActiva.snapshot.params != null){
      if(this.rutaActiva.snapshot.params.id != null){
        this.group.id_public = this.rutaActiva.snapshot.params.id;
        this.refresh();
        //this.getAnswer();
      }
    } 
  }


  refresh(){
    this._groupService.getGroup(this.group.id_public).subscribe(
      (data: ResponseModel) => { 
        let tuple:[GroupModel, number] = data.dataResponse;
        this.group = tuple['item1'];
        this.countGroups = tuple['item2'];
        console.log(this.group);
        this.question = this.group.questions[0];
        this.getAnswer();
      }
    );
  }

  getAnswer(){
    this._groupService.getAnswers(this.question.id_public).subscribe(
      (data:ResponseModel) =>{
        this.listAnswers = data.dataResponse;
        console.log(this.listAnswers);
      }
    );
  }

  confirm_delete(){
    this.message = "Esta seguro de querer eliminar la pregunta?";
    this.confirm_modal.openModal();
  }

  delete(){
    this._groupService.deleteQuestion(this.question.id_public).subscribe(
      data => this.refresh()
    );
  }

}
