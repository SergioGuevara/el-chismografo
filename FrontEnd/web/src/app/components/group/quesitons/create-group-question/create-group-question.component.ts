import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

//Services
import { GroupService } from '../../../../services/group.service';
import { QuestionService } from '../../../../services/question.service';

//Models
import { QuestionModel } from '../../../../models/question.model';
import { ResponseModel } from '../../../../models/util/response.model';

@Component({
  selector: 'app-create-group-question',
  templateUrl: './create-group-question.component.html',
  styleUrls: ['./create-group-question.component.css']
})
export class CreateGroupQuestionComponent implements OnInit {

  question:QuestionModel = new QuestionModel();

  constructor(
    private _groupService: GroupService,
    private _questionService: QuestionService,
    private rutaActiva: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.question = this._questionService.getQuestionModel();
    if(this.rutaActiva.snapshot.params != null){
      if(this.rutaActiva.snapshot.params.id != null){
        this.question.id_public_group = this.rutaActiva.snapshot.params.id;
      }
    } 
  }

  save(): void {
    this._groupService.createQuestion(this.question)
    .subscribe();
  }

}
