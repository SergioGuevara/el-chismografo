import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateGroupQuestionComponent } from './create-group-question.component';

describe('CreateGroupQuestionComponent', () => {
  let component: CreateGroupQuestionComponent;
  let fixture: ComponentFixture<CreateGroupQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateGroupQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGroupQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
