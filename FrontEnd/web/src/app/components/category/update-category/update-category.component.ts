import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

//Services
import { CategoryService } from '../../../services/category.service';

//Models
import { CategoryModel } from '../../../models/category.model';
import { ResponseModel } from '../../../models/util/response.model';

@Component({
  selector: 'app-update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.css']
})
export class UpdateCategoryComponent implements OnInit {

  category:CategoryModel;

  constructor(
    private _categoryService:CategoryService,
    private rutaActiva: ActivatedRoute
  ) { }

  ngOnInit(): void {
    if(this.rutaActiva.snapshot.params != null){
      if(this.rutaActiva.snapshot.params.id != null){
        this._categoryService.getCategory(this.rutaActiva.snapshot.params.id)
        .subscribe((data) => this.category = data["dataResponse"] );
      }
    }
  }

  update(){
    this._categoryService.edit(this.category).subscribe();
  }

}
