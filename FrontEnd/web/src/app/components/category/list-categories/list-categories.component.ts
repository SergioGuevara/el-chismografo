import { Component, OnInit } from '@angular/core';

//Services
import { CategoryService } from '../../../services/category.service';

//Models
import { CategoryModel } from '../../../models/category.model';
import { ResponseModel } from '../../../models/util/response.model';

@Component({
  selector: 'app-list-categories',
  templateUrl: './list-categories.component.html',
  styleUrls: ['./list-categories.component.css']
})
export class ListCategoriesComponent implements OnInit {

  listCategories: CategoryModel[] = [];

  constructor(
    private _categoryService: CategoryService
  ) { }

  ngOnInit(): void {
    this.refresh();
  }

  refresh(){
    this._categoryService.getAll().subscribe(
      data => this.listCategories = data["dataResponse"]
    );
  }

  changeStatus(id_public:string){
    this._categoryService.changeStatus(id_public).subscribe(
      data => this.refresh()
    );
  }

}
