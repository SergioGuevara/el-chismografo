import { Component, OnInit } from '@angular/core';

//Services
import { CategoryService } from '../../../services/category.service';

//Models
import { CategoryModel } from '../../../models/category.model';
import { ResponseModel } from '../../../models/util/response.model';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.css']
})
export class CreateCategoryComponent implements OnInit {

  category:CategoryModel = new CategoryModel();

  constructor(
    private _categoryService: CategoryService
  ) { }

  ngOnInit(): void {
  }

  save(): void {
    this._categoryService.createCategory(this.category)
    .subscribe();
  }

}
