import { UserModel } from './user.model'

export class AnswerModel
{
	constructor() {

    }
    
    id_public:string = null;
    text:string = null;
    status:number = 0;
    user:UserModel = new UserModel();
    created_at:string = null;
    updated_at:string = null;

    _id_public_question:string = null;
    
}
