import { UserModel } from "./user.model";
import { QuestionModel } from "./question.model";

export class GroupModel
{
	constructor() {

	}

    id_public:string = null;
    name: string = null;
    members: UserModel[] = [];
    questions: QuestionModel [] = [];
    creator_user: UserModel = null;//new UserModel();
    status:number = 0;
    created_at:string = null;
    updated_at:string = null;
    
}
