export class ResponseModel
{
	constructor() {

	}

    status: number = 0;
    dataResponse:any = null ;
    message: string = null;
    innerException: string = null;
    show:boolean = false;
}
