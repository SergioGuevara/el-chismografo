import { UserModel } from "./user.model";
import { GroupModel } from "./group.model";

export class InvitationModel
{
	constructor() {

	}

    invitation_type: number = 0;
    invitation_link: string = null;
    link_status: number = 0;
    group: GroupModel = new GroupModel();
    created_at:string = null;
    updated_at:string = null;
}
