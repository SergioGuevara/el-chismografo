import { CategoryModel } from "./category.model";
import { AnswerModel } from "./answer.model";

export class QuestionModel
{
	constructor() {
	}

    id_public:string = null;
    question:string = null; 
    category:CategoryModel = new CategoryModel();
    answers:AnswerModel [] = [];
    status:number = 0;
    created_at:string = null;
    updated_at:string = null;

    categories : CategoryModel[] = null;

    id_public_group:string = null;
}