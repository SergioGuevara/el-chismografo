import { UserModel } from "./user.model";

export class NotificationModel
{
	constructor() {

    }
    
    NotificationType: number = null;
    From: string = null;
    string: string[] = [];
    Subject: string = null;
    Body: string = null;
    statusNotification: number = null;
    User: UserModel = new UserModel();
    SentDate: string = null;
    Date: string = null;

    
}
