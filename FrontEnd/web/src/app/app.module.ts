import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxLocalStorageModule , NGX_LOCAL_STORAGE_SERIALIZER } from 'ngx-localstorage';

//********************************** COMPONENTS ********************************************

import { AppComponent } from './components/layouts/app/app.component';

//Question
import { CreateQuestionComponent } from './components/question/create-question/create-question.component';
import { ListQuestionComponent } from './components/question/list-question/list-question.component';
import { UpdateQuestionComponent } from './components/question/update-question/update-question.component';
//Category
import { CreateCategoryComponent } from './components/category/create-category/create-category.component';
import { ListCategoriesComponent } from './components/category/list-categories/list-categories.component';
import { UpdateCategoryComponent } from './components/category/update-category/update-category.component';
//Group
import { CreateGroupComponent } from './components/group/create-group/create-group.component';
import { UpdateGroupComponent } from './components/group/update-group/update-group.component';
import { ListGroupComponent } from './components/group/list-group/list-group.component';
//Group - Question
import { CreateGroupQuestionComponent } from './components/group/quesitons/create-group-question/create-group-question.component';
import { ListGroupQuestionComponent } from './components/group/quesitons/list-group-question/list-group-question.component';
import { UpdateGroupQuestionComponent } from './components/group/quesitons/update-group-question/update-group-question.component';

//********************************** SERVICES ********************************************

//Question
import { QuestionService } from './services/question.service';
//Category
import { CategoryService } from './services/category.service';
//Group
import { GroupService } from './services/group.service';
//User
import { UserService } from './services/user.service';
//Notification
import { NotificationService } from './services/notification.service';
// Shared
import { UserDataComponent } from './components/shared/user-data/user-data.component';

//********************************** MODULES ********************************************
import { AuthModule } from './auth/auth.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConfirmModalComponent } from './components/shared/confirm-modal/confirm-modal.component';

//********************************************************************************

@NgModule({
  declarations: [
    AppComponent,
    CreateQuestionComponent,
    CreateCategoryComponent,
    ListQuestionComponent,
    ListCategoriesComponent,
    UpdateCategoryComponent,
    UpdateQuestionComponent,
    CreateGroupComponent,
    UpdateGroupComponent,
    ListGroupComponent,
    CreateGroupQuestionComponent,
    ListGroupQuestionComponent,
    UpdateGroupQuestionComponent,
    UserDataComponent,
    ConfirmModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AuthModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    NgxLocalStorageModule.forRoot()
  ],
  providers: [
    QuestionService,
    CategoryService,
    GroupService,
    UserService,
    NotificationService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
