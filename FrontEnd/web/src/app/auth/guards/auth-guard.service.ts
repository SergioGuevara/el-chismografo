import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

//Services
import { AuthenticationService } from '../services/authentication.service'

@Injectable()
export class AuthGuardService implements CanActivate{

  //ATTRIBUTES
  response: any;
  current_url: string;

  //CONSTRUCTOR
  constructor(
      private _router:Router,
      private _authenticationService: AuthenticationService
   ) {
    this.current_url = location.pathname;
   }

  //METHODS
  canActivate(
    route: ActivatedRouteSnapshot, 
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree{
    console.log('authGUARD: ', this._authenticationService.statusLoggeo());
    if(this._authenticationService.statusLoggeo()){
      this.response = true;
    }else{
      this.response = this._router.navigateByUrl("/login?url="+ this.current_url);
    }
    return this.response
  }
  
}