import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

//Services
import { AuthenticationService } from '../services/authentication.service'

@Injectable()
export class AnonymousGuardService implements CanActivate{

  //ATTRIBUTES
  response: any;

  //CONSTRUCTOR
  constructor(
      private _router:Router,
      private _authenticationService: AuthenticationService
   ) {
   }

  //METHODS
  canActivate(
    route: ActivatedRouteSnapshot, 
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree{
    console.log('anonymousGUARD: ', this._authenticationService.statusLoggeo());
    if(!this._authenticationService.statusLoggeo()){
      this.response = true;
    }else{
      this.response = this._router.navigateByUrl("/notes");
    }
    return this.response
  }
  
}