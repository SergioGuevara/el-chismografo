import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

//Services
import { AuthenticationService } from '../../services/authentication.service'

//Models
import { ResponseModel } from '../../models/util/response.model'
import { enumTypeMessageResponse, enumAuthProvider } from '../../models/util/Enumerations.enum'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  redirection_url: string;

  constructor(
    private _rutaActiva: ActivatedRoute,
    private _router: Router,
    private _authenticationService: AuthenticationService
  ) { 
  }

  ngOnInit(): void {
    if(this._rutaActiva.snapshot.params != null){
      if(this._rutaActiva.snapshot.queryParamMap.get("url") != null){
        this.redirection_url = this._rutaActiva.snapshot.queryParamMap.get("url");        
      }
    }
  }

  async signInWithGoogle() {
    await this._authenticationService.SignByAuth(enumAuthProvider.Google);
    this.register();
  }
 
  async signInWithFB() {
    await this._authenticationService.SignByAuth(enumAuthProvider.Facebook);
    this.register();
  }

  register(){
    if(this._authenticationService.socialLogeo){
      this._authenticationService.register().subscribe(
        (data:ResponseModel) => {
          if(data.status == enumTypeMessageResponse.Success){
            this._authenticationService.setUser(data.dataResponse);
            this.redirect();
          }else{
            this._authenticationService.login().subscribe(
              (data:ResponseModel) => {
                if(data.status == enumTypeMessageResponse.Success){
                  this._authenticationService.setUser(data.dataResponse);
                  this.redirect();
                }else{
                  this._authenticationService.Signout();
                }
              }
            );
          }
        }
      );
    }else{
      this._authenticationService.Signout();
    }
  }

  private redirect(){
    this._router.navigateByUrl(this.redirection_url);
  }

}
