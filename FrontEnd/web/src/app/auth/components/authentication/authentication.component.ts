import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

//Services
import { AuthenticationService } from '../../services/authentication.service'

//Models
import { ResponseModel } from '../../models/util/response.model'
import { enumTypeMessageResponse, enumAuthProvider } from '../../models/util/Enumerations.enum'

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  redirection_url: string;

  constructor(
    private _rutaActiva: ActivatedRoute,
    private _router: Router,
    private _authenticationService: AuthenticationService
  ) { 
  }

  ngOnInit(): void {
    if(this._rutaActiva.snapshot.params != null){
      if(this._rutaActiva.snapshot.queryParamMap.get("url") != null){
        this.redirection_url = this._rutaActiva.snapshot.queryParamMap.get("url");
        console.log('login:' , this.redirection_url);    
      }
    }else{
      this.redirection_url = '/notes';
    }
  }

  async signInWithGoogle() {
    await this._authenticationService.SignByAuth(enumAuthProvider.Google);
    this.login();
  }
 
  async signInWithFB() {
    await this._authenticationService.SignByAuth(enumAuthProvider.Facebook);
    this.login();
  }

  login(){
    if(this._authenticationService.socialLogeo){
      this._authenticationService.login().subscribe(
        (data:ResponseModel) => {
          if(data.status == enumTypeMessageResponse.Success){
            console.log('logeo exitoso');
            this._authenticationService.setUser(data.dataResponse);
            console.log('logeo exitoso 2');
            this.redirect();
          }else{
            console.log('error loego');
            this._authenticationService.Signout();
          }
        }
      );
    }else{
      this._authenticationService.Signout();
    }
  }

  private redirect(){
    console.log('redirectTo:' , this.redirection_url);
    this._router.navigateByUrl(this.redirection_url);
  }

}
