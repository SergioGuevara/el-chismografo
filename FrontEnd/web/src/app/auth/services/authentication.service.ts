import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import {LocalStorageService} from 'ngx-localstorage';

//Models Imported
import { ResponseModel } from '../../models/util/response.model';
import { UserModel } from '../../models/user.model';
import { enumAuthProvider } from '../models/util/Enumerations.enum'
// Login Social 
import { SocialAuthService, SocialUser } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class AuthenticationService {

  //ATTRIBUTES
  public statusLoggeo(): boolean { 
    return this._storageService.get("user") != null ? true : false ;
  }
  public socialLogeo: boolean = false;
  private url:string;
  private response: ResponseModel;

  //CONSTRUCTOR
  constructor( 
    private authService: SocialAuthService,
    private http: HttpClient,
    private _storageService: LocalStorageService
    ) {
      this.url = `${ environment.rootBaseApi }`;
  }

  //METHODS
  async SignByAuth(provider:enumAuthProvider) {
    switch(provider){
      case enumAuthProvider.Google:
        await this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then( x => {
          this.getSocialUser();
        });
        break;
      case enumAuthProvider.Facebook:
        await this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then( x => {
          this.getSocialUser();
        });
        break;
    }
  }

  private getSocialUser(){
    this.authService.authState.subscribe((user) => {
      this._storageService.set("socialUser", user);
      this.socialLogeo = true;
    });
  }

  getUser(): UserModel{
    return this._storageService.get("user");
  }

  setUser(user: UserModel){
    console.log('setiando el usuario');
    this._storageService.set("user", user);
    console.log(this._storageService.get("user"));
  }

  register(): Observable<ResponseModel>{
    return this.http.post<ResponseModel>(`${this.url}user/register`, this._storageService.get('socialUser') , httpOptions).pipe(
      );
  }

  login(): Observable<ResponseModel> {
    return this.http.post<ResponseModel>(`${this.url}user/login`, this._storageService.get('socialUser'), httpOptions).pipe(
      );
  }

  Signout(){
    console.log('0');
    if(this.statusLoggeo){
      console.log('1');
      this.LogOutUser().subscribe();
      console.log('2');
      this.authService.signOut();
      console.log('3');
      this._storageService.remove('socialUser');
      console.log('4');
      this._storageService.remove('user');
      console.log('5');
    }
  }

  private LogOutUser(): Observable<ResponseModel> {
    return this.http.post<ResponseModel>(`${this.url}user/logout`, this._storageService.get('socialUser'), httpOptions).pipe(
    );
  }
  
}