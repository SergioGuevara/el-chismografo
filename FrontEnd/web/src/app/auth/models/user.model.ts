
export class UserModel
{
	constructor() {

	}

    id_public:string = null;
    document_type_id:string = null;
    identification:string = null;
    name:string = null;
    lastname:string = null;
    celphone:string = null;
    photo:string = null;
    email:string = null;
    status:number = 0;
    created_at:string = null;
    updated_at:string = null;

}
