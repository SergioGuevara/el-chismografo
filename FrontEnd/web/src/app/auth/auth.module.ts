import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//********************************** COMPONENTS ********************************************
import { AuthenticationComponent} from './components/authentication/authentication.component';
import { RegisterComponent } from './components/register/register.component';

//********************************** LIBRARIES ********************************************
import { SocialLoginModule, SocialAuthServiceConfig  } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';

//********************************** SERVICES ********************************************
import { AuthenticationService } from './services/authentication.service';
import { AuthGuardService } from './guards/auth-guard.service';
import { AnonymousGuardService } from './guards/anonymous-guard.service';

@NgModule({
  declarations: [
    AuthenticationComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
    SocialLoginModule,
    AuthGuardService,
    AnonymousGuardService,
    AuthenticationService,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: true,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              //'940215792993-omc70o0270f8sv5v8m6givnh0gpmjejq.apps.googleusercontent.com'
              '940215792993-hhqnlpo8dojr449906kd7jmusqmngpab.apps.googleusercontent.com'
            ),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('892030211296710'),
          },
        ],
      } as SocialAuthServiceConfig,
    }
  ],
  exports: [
    AuthenticationComponent,
    RegisterComponent,
    SocialLoginModule
  ]
})
export class AuthModule { }
