import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//********************************** COMPONENTS ********************************************
//Question
import { CreateQuestionComponent } from './components/question/create-question/create-question.component';
import { ListQuestionComponent } from './components/question/list-question/list-question.component';
import { UpdateQuestionComponent } from './components/question/update-question/update-question.component';
//Category
import { CreateCategoryComponent } from './components/category/create-category/create-category.component';
import { ListCategoriesComponent } from './components/category/list-categories/list-categories.component';
import { UpdateCategoryComponent } from './components/category/update-category/update-category.component';
//Group
import { CreateGroupComponent } from './components/group/create-group/create-group.component';
import { UpdateGroupComponent } from './components/group/update-group/update-group.component';
import { ListGroupComponent } from './components/group/list-group/list-group.component';
//Group - Question
import { CreateGroupQuestionComponent } from './components/group/quesitons/create-group-question/create-group-question.component';
import { ListGroupQuestionComponent } from './components/group/quesitons/list-group-question/list-group-question.component';
import { UpdateGroupQuestionComponent } from './components/group/quesitons/update-group-question/update-group-question.component';
// Login - Register - Authentication
import { AuthenticationComponent } from './auth/components/authentication/authentication.component';
import { RegisterComponent } from './auth/components/register/register.component';
import { AuthGuardService } from './auth/guards/auth-guard.service';
import { AnonymousGuardService } from './auth/guards/anonymous-guard.service';

const routes: Routes = [

  { path: "" , redirectTo: "notes", pathMatch: "full" },

  //Login - Register - Authentication
  { path: "login" , component: AuthenticationComponent , canActivate: [AnonymousGuardService]},
  { path: "register" , component: RegisterComponent , canActivate: [AnonymousGuardService] },

  //Question
  { path: "questions" , component: ListQuestionComponent},
  { path: "question/:id/edit" , component: UpdateQuestionComponent },
  { path: "question/create" , component: CreateQuestionComponent },

  //Category
  { path: "categories" , component: ListCategoriesComponent }, // },
  { path: "category/:id/edit" , component: UpdateCategoryComponent },
  { path: "category/create" , component: CreateCategoryComponent },
  
  //Group
  { path: "notes" , component: ListGroupComponent , canActivate: [AuthGuardService] },
  { path: "note/:id/edit" , component: UpdateGroupComponent },
  { path: "note/create" , component: CreateGroupComponent },
  //Group-Question
  { path: "note/:id/questions" , component: ListGroupQuestionComponent },
  { path: "note/:id/question/:question/edit" , component: UpdateGroupQuestionComponent },
  { path: "note/:id/question/create" , component: CreateGroupQuestionComponent },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
